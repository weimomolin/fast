/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : fast

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 08/11/2023 16:28:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fa_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `fa_auth_rule`;
CREATE TABLE `fa_auth_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` enum('menu','file') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '规则名称',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '图标',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '规则URL',
  `condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为菜单',
  `menutype` enum('addtabs','blank','dialog','ajax') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单类型',
  `extend` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '扩展属性',
  `py` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '拼音首字母',
  `pinyin` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '拼音',
  `createtime` bigint(16) NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` bigint(16) NULL DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT 0 COMMENT '权重',
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE,
  INDEX `weigh`(`weigh`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 157 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '节点表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_auth_rule
-- ----------------------------
INSERT INTO `fa_auth_rule` VALUES (1, 'file', 0, 'dashboard', 'Dashboard', 'fa fa-dashboard', '', '', 'Dashboard tips', 1, NULL, '', 'kzt', 'kongzhitai', 1491635035, 1491635035, 143, 'normal');
INSERT INTO `fa_auth_rule` VALUES (2, 'file', 0, 'general', 'General', 'fa fa-cogs', '', '', '', 1, NULL, '', 'cggl', 'changguiguanli', 1491635035, 1491635035, 137, 'normal');
INSERT INTO `fa_auth_rule` VALUES (3, 'file', 0, 'category', 'Category', 'fa fa-leaf', '', '', 'Category tips', 0, NULL, '', 'flgl', 'fenleiguanli', 1491635035, 1491635035, 119, 'normal');
INSERT INTO `fa_auth_rule` VALUES (4, 'file', 0, 'addon', 'Addon', 'fa fa-rocket', '', '', 'Addon tips', 1, NULL, '', 'cjgl', 'chajianguanli', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (5, 'file', 0, 'auth', 'Auth', 'fa fa-group', '', '', '', 1, NULL, '', 'qxgl', 'quanxianguanli', 1491635035, 1491635035, 99, 'normal');
INSERT INTO `fa_auth_rule` VALUES (6, 'file', 2, 'general/config', 'Config', 'fa fa-cog', '', '', 'Config tips', 1, NULL, '', 'xtpz', 'xitongpeizhi', 1491635035, 1491635035, 60, 'normal');
INSERT INTO `fa_auth_rule` VALUES (7, 'file', 2, 'general/attachment', 'Attachment', 'fa fa-file-image-o', '', '', 'Attachment tips', 1, NULL, '', 'fjgl', 'fujianguanli', 1491635035, 1491635035, 53, 'normal');
INSERT INTO `fa_auth_rule` VALUES (8, 'file', 2, 'general/profile', 'Profile', 'fa fa-user', '', '', '', 1, NULL, '', 'grzl', 'gerenziliao', 1491635035, 1491635035, 34, 'normal');
INSERT INTO `fa_auth_rule` VALUES (9, 'file', 5, 'auth/admin', 'Admin', 'fa fa-user', '', '', 'Admin tips', 1, NULL, '', 'glygl', 'guanliyuanguanli', 1491635035, 1491635035, 118, 'normal');
INSERT INTO `fa_auth_rule` VALUES (10, 'file', 5, 'auth/adminlog', 'Admin log', 'fa fa-list-alt', '', '', 'Admin log tips', 1, NULL, '', 'glyrz', 'guanliyuanrizhi', 1491635035, 1491635035, 113, 'normal');
INSERT INTO `fa_auth_rule` VALUES (11, 'file', 5, 'auth/group', 'Group', 'fa fa-group', '', '', 'Group tips', 1, NULL, '', 'jsz', 'juesezu', 1491635035, 1491635035, 109, 'normal');
INSERT INTO `fa_auth_rule` VALUES (12, 'file', 5, 'auth/rule', 'Rule', 'fa fa-bars', '', '', 'Rule tips', 1, NULL, '', 'cdgz', 'caidanguize', 1491635035, 1491635035, 104, 'normal');
INSERT INTO `fa_auth_rule` VALUES (13, 'file', 1, 'dashboard/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 136, 'normal');
INSERT INTO `fa_auth_rule` VALUES (14, 'file', 1, 'dashboard/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 135, 'normal');
INSERT INTO `fa_auth_rule` VALUES (15, 'file', 1, 'dashboard/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 133, 'normal');
INSERT INTO `fa_auth_rule` VALUES (16, 'file', 1, 'dashboard/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 134, 'normal');
INSERT INTO `fa_auth_rule` VALUES (17, 'file', 1, 'dashboard/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 132, 'normal');
INSERT INTO `fa_auth_rule` VALUES (18, 'file', 6, 'general/config/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 52, 'normal');
INSERT INTO `fa_auth_rule` VALUES (19, 'file', 6, 'general/config/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 51, 'normal');
INSERT INTO `fa_auth_rule` VALUES (20, 'file', 6, 'general/config/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 50, 'normal');
INSERT INTO `fa_auth_rule` VALUES (21, 'file', 6, 'general/config/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 49, 'normal');
INSERT INTO `fa_auth_rule` VALUES (22, 'file', 6, 'general/config/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 48, 'normal');
INSERT INTO `fa_auth_rule` VALUES (23, 'file', 7, 'general/attachment/index', 'View', 'fa fa-circle-o', '', '', 'Attachment tips', 0, NULL, '', '', '', 1491635035, 1491635035, 59, 'normal');
INSERT INTO `fa_auth_rule` VALUES (24, 'file', 7, 'general/attachment/select', 'Select attachment', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 58, 'normal');
INSERT INTO `fa_auth_rule` VALUES (25, 'file', 7, 'general/attachment/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 57, 'normal');
INSERT INTO `fa_auth_rule` VALUES (26, 'file', 7, 'general/attachment/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 56, 'normal');
INSERT INTO `fa_auth_rule` VALUES (27, 'file', 7, 'general/attachment/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 55, 'normal');
INSERT INTO `fa_auth_rule` VALUES (28, 'file', 7, 'general/attachment/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 54, 'normal');
INSERT INTO `fa_auth_rule` VALUES (29, 'file', 8, 'general/profile/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 33, 'normal');
INSERT INTO `fa_auth_rule` VALUES (30, 'file', 8, 'general/profile/update', 'Update profile', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 32, 'normal');
INSERT INTO `fa_auth_rule` VALUES (31, 'file', 8, 'general/profile/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 31, 'normal');
INSERT INTO `fa_auth_rule` VALUES (32, 'file', 8, 'general/profile/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 30, 'normal');
INSERT INTO `fa_auth_rule` VALUES (33, 'file', 8, 'general/profile/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 29, 'normal');
INSERT INTO `fa_auth_rule` VALUES (34, 'file', 8, 'general/profile/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 28, 'normal');
INSERT INTO `fa_auth_rule` VALUES (35, 'file', 3, 'category/index', 'View', 'fa fa-circle-o', '', '', 'Category tips', 0, NULL, '', '', '', 1491635035, 1491635035, 142, 'normal');
INSERT INTO `fa_auth_rule` VALUES (36, 'file', 3, 'category/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 141, 'normal');
INSERT INTO `fa_auth_rule` VALUES (37, 'file', 3, 'category/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 140, 'normal');
INSERT INTO `fa_auth_rule` VALUES (38, 'file', 3, 'category/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 139, 'normal');
INSERT INTO `fa_auth_rule` VALUES (39, 'file', 3, 'category/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 138, 'normal');
INSERT INTO `fa_auth_rule` VALUES (40, 'file', 9, 'auth/admin/index', 'View', 'fa fa-circle-o', '', '', 'Admin tips', 0, NULL, '', '', '', 1491635035, 1491635035, 117, 'normal');
INSERT INTO `fa_auth_rule` VALUES (41, 'file', 9, 'auth/admin/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 116, 'normal');
INSERT INTO `fa_auth_rule` VALUES (42, 'file', 9, 'auth/admin/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 115, 'normal');
INSERT INTO `fa_auth_rule` VALUES (43, 'file', 9, 'auth/admin/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 114, 'normal');
INSERT INTO `fa_auth_rule` VALUES (44, 'file', 10, 'auth/adminlog/index', 'View', 'fa fa-circle-o', '', '', 'Admin log tips', 0, NULL, '', '', '', 1491635035, 1491635035, 112, 'normal');
INSERT INTO `fa_auth_rule` VALUES (45, 'file', 10, 'auth/adminlog/detail', 'Detail', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 111, 'normal');
INSERT INTO `fa_auth_rule` VALUES (46, 'file', 10, 'auth/adminlog/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 110, 'normal');
INSERT INTO `fa_auth_rule` VALUES (47, 'file', 11, 'auth/group/index', 'View', 'fa fa-circle-o', '', '', 'Group tips', 0, NULL, '', '', '', 1491635035, 1491635035, 108, 'normal');
INSERT INTO `fa_auth_rule` VALUES (48, 'file', 11, 'auth/group/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 107, 'normal');
INSERT INTO `fa_auth_rule` VALUES (49, 'file', 11, 'auth/group/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 106, 'normal');
INSERT INTO `fa_auth_rule` VALUES (50, 'file', 11, 'auth/group/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 105, 'normal');
INSERT INTO `fa_auth_rule` VALUES (51, 'file', 12, 'auth/rule/index', 'View', 'fa fa-circle-o', '', '', 'Rule tips', 0, NULL, '', '', '', 1491635035, 1491635035, 103, 'normal');
INSERT INTO `fa_auth_rule` VALUES (52, 'file', 12, 'auth/rule/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 102, 'normal');
INSERT INTO `fa_auth_rule` VALUES (53, 'file', 12, 'auth/rule/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 101, 'normal');
INSERT INTO `fa_auth_rule` VALUES (54, 'file', 12, 'auth/rule/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 100, 'normal');
INSERT INTO `fa_auth_rule` VALUES (55, 'file', 4, 'addon/index', 'View', 'fa fa-circle-o', '', '', 'Addon tips', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (56, 'file', 4, 'addon/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (57, 'file', 4, 'addon/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (58, 'file', 4, 'addon/del', 'Delete', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (59, 'file', 4, 'addon/downloaded', 'Local addon', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (60, 'file', 4, 'addon/state', 'Update state', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (63, 'file', 4, 'addon/config', 'Setting', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (64, 'file', 4, 'addon/refresh', 'Refresh', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (65, 'file', 4, 'addon/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (66, 'file', 0, 'user', 'User', 'fa fa-user-circle', '', '', '', 1, NULL, '', 'hygl', 'huiyuanguanli', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (67, 'file', 66, 'user/user', 'User', 'fa fa-user', '', '', '', 1, NULL, '', 'hygl', 'huiyuanguanli', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (68, 'file', 67, 'user/user/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (69, 'file', 67, 'user/user/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (70, 'file', 67, 'user/user/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (71, 'file', 67, 'user/user/del', 'Del', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (72, 'file', 67, 'user/user/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (73, 'file', 66, 'user/group', 'User group', 'fa fa-users', '', '', '', 1, NULL, '', 'hyfz', 'huiyuanfenzu', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (74, 'file', 73, 'user/group/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (75, 'file', 73, 'user/group/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (76, 'file', 73, 'user/group/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (77, 'file', 73, 'user/group/del', 'Del', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (78, 'file', 73, 'user/group/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (79, 'file', 66, 'user/rule', 'User rule', 'fa fa-circle-o', '', '', '', 1, NULL, '', 'hygz', 'huiyuanguize', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (80, 'file', 79, 'user/rule/index', 'View', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (81, 'file', 79, 'user/rule/del', 'Del', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (82, 'file', 79, 'user/rule/add', 'Add', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (83, 'file', 79, 'user/rule/edit', 'Edit', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (84, 'file', 79, 'user/rule/multi', 'Multi', 'fa fa-circle-o', '', '', '', 0, NULL, '', '', '', 1491635035, 1491635035, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (88, 'file', 0, 'subject', '云课堂', 'fa fa-university', '', '', '', 1, 'addtabs', '', 'ykt', 'yunketang', 1697527607, 1697527704, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (89, 'file', 88, 'subject/subject', '课程管理', 'fa fa-calculator', '', '', '', 1, 'addtabs', '', 'kcgl', 'kechengguanli', 1697527626, 1697527714, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (90, 'file', 89, 'subject/Subject/add', '添加', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'tj', 'tianjia', 1697528746, 1697528746, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (91, 'file', 89, 'subject/Subject/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'bj', 'bianji', 1697528765, 1697528796, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (92, 'file', 89, 'subject/Subject/del', '软删除', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'rsc', 'ruanshanchu', 1697528788, 1697528788, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (93, 'file', 89, 'subject/Subject/index', '查看', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'zk', 'zhakan', 1697528827, 1697528827, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (94, 'file', 88, 'subject/recyclebin', '回收站', 'fa fa-undo', '', '', '', 1, 'addtabs', '', 'hsz', 'huishouzhan', 1697529281, 1697599218, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (95, 'file', 94, 'subject/recycle/restore', '恢复', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'hf', 'huifu', 1697599260, 1697599260, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (96, 'file', 94, 'subject/recycle/destroy', '销毁', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'xh', 'xiaohui', 1697599281, 1697599281, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (97, 'file', 89, 'subject/Chapter', '课程章节', 'fa fa-bars', '', '', '', 0, 'addtabs', '', 'kczj', 'kechengzhangjie', 1697614202, 1697620195, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (98, 'file', 88, 'subject/category', '课程分类', 'fa fa-align-justify', '', '', '', 1, 'addtabs', '', 'kcfl', 'kechengfenlei', 1697695541, 1697695598, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (100, 'file', 98, 'subject/Category/index', '分类列表', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'fllb', 'fenleiliebiao', 1697696833, 1697698084, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (101, 'file', 98, 'subject/Category/add', '添加', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'tj', 'tianjia', 1697696875, 1697698096, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (102, 'file', 98, 'subject/Category/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'bj', 'bianji', 1697696910, 1697698105, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (103, 'file', 98, 'subject/CSategory/del', '删除', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'sc', 'shanchu', 1697696947, 1697698114, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (104, 'file', 88, 'subject/order', '课程订单', 'fa fa-building-o', '', '', '', 1, 'addtabs', '', 'kcdd', 'kechengdingdan', 1697701810, 1697701810, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (105, 'file', 104, 'subject/order/index', '订单列表', 'fa fa-building', '', '', '', 0, 'addtabs', '', 'ddlb', 'dingdanliebiao', 1697701864, 1697701874, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (106, 'file', 104, 'subject/order/del', '软删除', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'rsc', 'ruanshanchu', 1697701915, 1697701915, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (107, 'file', 0, 'buiness', '客户中心', 'fa fa-users', '', '', '', 1, 'addtabs', '', 'khzx', 'kehuzhongxin', 1697784134, 1697784134, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (108, 'file', 107, 'business/source', '客户来源', 'fa fa-circle-o', '', '', '', 1, 'addtabs', '', 'khly', 'kehulaiyuan', 1697784164, 1697784176, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (109, 'file', 107, 'business/highsea', '客户公海', 'fa fa-circle-o', '', '', '', 1, 'addtabs', '', 'khgh', 'kehugonghai', 1697784192, 1697784192, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (110, 'file', 107, 'business/privatesea', '客户私海', 'fa fa-circle-o', '', '', '', 1, 'addtabs', '', 'khsh', 'kehusihai', 1697784209, 1697784209, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (111, 'file', 107, 'business/budrecycle', '客户回收站', 'fa fa-circle-o', '', '', '', 1, 'addtabs', '', 'khhsz', 'kehuhuishouzhan', 1697784229, 1697784229, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (112, 'file', 108, 'business/source/index', '来源列表', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'lylb', 'laiyuanliebiao', 1697784260, 1697784260, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (113, 'file', 108, 'business/source/add', '添加', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'tj', 'tianjia', 1697787236, 1697787236, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (114, 'file', 108, 'business/source/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'bj', 'bianji', 1697787263, 1697787263, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (115, 'file', 108, 'business/source/del', '删除', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'sc', 'shanchu', 1697787280, 1697787280, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (116, 'file', 0, 'product', '商品管理', 'fa fa-shopping-bag', '', '', '', 1, 'addtabs', '', 'spgl', 'shangpinguanli', 1697873564, 1697873564, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (117, 'file', 116, 'product/type', '商品分类', 'fa fa-circle-o', '', '', '', 1, 'addtabs', '', 'spfl', 'shangpinfenlei', 1697873611, 1697873711, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (118, 'file', 116, 'product/unit', '商品单位', 'fa fa-circle-o', '', '', '', 1, 'addtabs', '', 'spdw', 'shangpindanwei', 1697873642, 1697873715, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (119, 'file', 116, 'product/product', '商品管理', 'fa fa-circle-o', '', '', '', 1, 'addtabs', '', 'spgl', 'shangpinguanli', 1697873662, 1697873720, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (120, 'file', 116, 'product/order', '订单管理', 'fa fa-circle-o', '', '', '', 1, 'addtabs', '', 'ddgl', 'dingdanguanli', 1697873682, 1697873722, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (121, 'file', 116, 'product/recyclebin/index', '回收站', 'fa fa-circle-o', '', '', '', 1, 'addtabs', '', 'hsz', 'huishouzhan', 1697873706, 1697873725, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (122, 'file', 117, 'product/type/index', '列表', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'lb', 'liebiao', 1697880546, 1697880546, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (123, 'file', 117, 'product/type/add', '新增', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'xz', 'xinzeng', 1697880567, 1697880567, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (124, 'file', 117, 'product/type/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'bj', 'bianji', 1697880591, 1697880591, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (125, 'file', 117, 'product/type/del', '删除', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'sc', 'shanchu', 1697880608, 1697880608, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (126, 'file', 117, 'product/type/multi', '批量操作', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'plcz', 'piliangcaozuo', 1697880625, 1697880625, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (127, 'file', 118, 'product/unit/index', '查看', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'zk', 'zhakan', 1697880649, 1697880649, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (128, 'file', 118, 'product/unit/add', '添加', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'tj', 'tianjia', 1697880658, 1697880717, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (129, 'file', 118, 'product/unit/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'bj', 'bianji', 1697880677, 1697880677, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (130, 'file', 118, 'product/unit/del', '删除', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'sc', 'shanchu', 1697880690, 1697880690, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (131, 'file', 118, 'product/unit/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'plgx', 'pilianggengxin', 1697880738, 1697880746, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (132, 'file', 119, 'product/product/index', '查看', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'zk', 'zhakan', 1697880768, 1697880768, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (133, 'file', 119, 'product/product/add', '添加', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'tj', 'tianjia', 1697880780, 1697880780, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (134, 'file', 119, 'product/product/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'bj', 'bianji', 1697880794, 1697880794, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (135, 'file', 119, 'product/product/del', '删除', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'sc', 'shanchu', 1697880806, 1697880806, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (136, 'file', 119, 'product/product/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'plgx', 'pilianggengxin', 1697880820, 1697880820, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (137, 'file', 0, 'depot', '进销存管理', 'fa fa-cubes', '', '', '', 1, 'addtabs', '', 'jxcgl', 'jinxiaocunguanli', 1697881087, 1697881117, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (138, 'file', 137, 'depot/supplier', '供应商', 'fa fa-ambulance', '', '', '', 1, 'addtabs', '', 'gys', 'gongyingshang', 1697881141, 1697881141, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (139, 'file', 138, 'depot/supplier/index', '列表', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'lb', 'liebiao', 1697881173, 1697881187, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (140, 'file', 138, 'depot/supplier/add', '添加', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'tj', 'tianjia', 1697881198, 1697881198, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (141, 'file', 138, 'depot/supplier/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'bj', 'bianji', 1697881210, 1697881210, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (142, 'file', 138, 'depot/supplier/del', '删除', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'sc', 'shanchu', 1697881222, 1697881222, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (143, 'file', 138, 'depot/supplier/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'plgx', 'pilianggengxin', 1697881237, 1697881237, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (144, 'file', 137, 'depot/storage', '入库管理', 'fa fa-cube', '', '', '', 1, 'addtabs', '', 'rkgl', 'rukuguanli', 1697881285, 1697881285, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (145, 'file', 144, 'depot/storage/index', '查看', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'zk', 'zhakan', 1697881298, 1697881326, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (146, 'file', 144, 'depot/storage/add', '添加', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'tj', 'tianjia', 1697881344, 1697881344, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (147, 'file', 144, 'depot/storage/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'bj', 'bianji', 1697881359, 1697881359, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (148, 'file', 144, 'depot/storage/del', '删除', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'sc', 'shanchu', 1697881370, 1697881370, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (149, 'file', 144, 'depot/storage/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'plgx', 'pilianggengxin', 1697881384, 1697881384, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (150, 'file', 137, 'depot/back', '退货管理', 'fa fa-reply', '', '', '', 1, 'addtabs', '', 'thgl', 'tuihuoguanli', 1697881475, 1697881475, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (151, 'file', 150, 'depot/back/index', '查看', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'zk', 'zhakan', 1697881502, 1697881502, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (152, 'file', 150, 'depot/back/add', '新增', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'xz', 'xinzeng', 1697881528, 1697881528, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (153, 'file', 150, 'depot/back/edit', '编辑', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'bj', 'bianji', 1697881543, 1697881543, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (154, 'file', 150, 'depot/back/del', '删除', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'sc', 'shanchu', 1697881556, 1697881556, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (155, 'file', 150, 'depot/back/multi', '批量更新', 'fa fa-circle-o', '', '', '', 0, 'addtabs', '', 'plgx', 'pilianggengxin', 1697881572, 1697881572, 0, 'normal');
INSERT INTO `fa_auth_rule` VALUES (156, 'file', 137, 'depot/recyclebin/index', '回收站', 'fa fa-exchange', '', '', '', 1, 'addtabs', '', 'hsz', 'huishouzhan', 1697881591, 1697881629, 0, 'normal');

SET FOREIGN_KEY_CHECKS = 1;
