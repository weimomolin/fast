<?php

namespace app\hotel\controller;

use think\Controller;


class Room extends Controller
{
    public function __construct()
    {
        parent::__construct();   

        $this->BusinessModel = model('Business.Business');

        $this->CouponModel = model('Hotel.Coupon.Coupon');
        $this->GuestModel = model('Hotel.Guest');

        $this->ReceiveModel = model('Hotel.Coupon.Receive');

        $this->OrderModel = model('Hotel.Order');

        $this->model = model('Hotel.Room');
    }

    public function index()
    {
        if($this->request->isPost())
        {
            $page = $this->request->param('page', '1', 'trim');
            $keywords = $this->request->param('keywords', '', 'trim');
            $limit = 10;
            $start = ($page-1)*$limit;

            $where = [];

            if(!empty($keywords))
            {
                $where['name'] = ['LIKE', "%$keywords%"];
            }

            $room = $this->model
                ->where($where)
                ->limit($start, $limit)
                ->select();

            if($room)
            {
                $this->success('返回列表', null, $room);
                exit;
            }else
            {
                $this->error('暂无数据');
                exit;
            }
        }
    }

    public function info()
    {
        if($this->request->isPost())
        {
            $rid = $this->request->param('rid', 0, 'trim');

            //查询房间是否存在
            $room = $this->model->find($rid);

            if(!$room)
            {
                $this->error('暂无房间信息');
                exit;
            }

            //查询一下房间的订单
            $where = [
                'roomid' => $rid,
                'status' => ['IN', ['1', '2']],
            ];

            $count = $this->OrderModel->where($where)->count();

            //在数据中插入一个自定义的属性，用来表示是否可以预订
            $room['state'] = bcsub($room['total'], $count) <= 0 ? false : true;

            $this->success('返回房间信息', null, $room);
            exit;
        }
    }

    public function guest()
    {
        if($this->request->isPost())
        {
            $busid = $this->request->param('busid', 0, 'trim');

            $list = $this->GuestModel->where(['busid' => $busid])->select();

            if($list)
            {
                $this->success('返回住客信息', null, $list);
                exit;
            }else
            {
                $this->error('暂无住客信息');
                exit;
            }
        }
    }

    

   
}
