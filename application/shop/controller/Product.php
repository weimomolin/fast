<?php

namespace app\shop\controller;

use think\Controller;

class Product extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->ProductModel = model('Product.Product');
        $this->TypeModel = model('Product.Type');
        $this->CartModel = model('Product.Cart');
    }

    //查询分类对应的商品数据
    public function tabulation()
    {
        if ($this->request->isPost()) {
            //分类
            $typeid = $this->request->param("typeid", 0, 'trim');
            if ($typeid != 0) {
                $productList = $this->ProductModel->where(['typeid' => $typeid])->select();
            } else {
                $productList = $this->ProductModel->select();
            }
            if ($productList) {
                $this->success('', null, $productList);
                exit;
            } else {
                $this->error('暂无商品');
                exit;
            }
        }
    }
    // 首页头部右侧分类数据
    public function type()
    {
        if ($this->request->isPost()) {
            //分类
            $type = $this->TypeModel->with('product')->select();

            $this->success('分类数据', null, $type);
            exit;
        }
    }
}
