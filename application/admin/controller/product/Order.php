<?php

namespace app\admin\controller\product;

use app\common\controller\Backend;
use Exception;
use think\Db;

/**
 * 订单管理
 *
 * @icon fa fa-circle-o
 */
class Order extends Backend
{
    // 开启关联查询
    protected $relationSearch = true;

    // 快速搜素字段
    protected $searchFields = ['code','business.nickname','express.name','expresscode'];

    /**
     * Order模型对象
     * @var \app\common\model\Product\Order
     */
    protected $model = null;
    
    // 订单商品模型
    protected $OrderProductModel = null;

    public function _initialize()
    {
        parent::_initialize();

        // 加载模型
        $this->model = new \app\common\model\Product\order\Order;
        $this->OrderProductModel = model('Product.order.Product');


        $this->view->assign("statusList", $this->model->getStatusList());
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);

        // 判断是否有Ajax请求
        if($this->request->isAjax())
        {
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }

            [$where, $sort, $order, $offset, $limit] = $this->buildparams();

            $list = $this->model
                ->with(['express','business'])
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = ['total' => $list->total(), 'rows' => $list->items()];

            return json($result);
        }

        // 输出视图
        return $this->view->fetch();
    }

    public function info($ids = null)
    {
        // 关联查询
        $row = $this->model->with(['business','express','address' => ['provinces','citys','districts'],'sale','review','dispatched'])->find($ids);

        if(!$row)
        {
            $this->error('订单不存在');
        }

        $OrderProductData = $this->OrderProductModel->with(['products'])->where(['orderid' => $ids])->select();

        $this->assign([
            'row' => $row,
            'OrderProductData' => $OrderProductData
        ]);

        return $this->fetch();
    }

    // 发货
    public function deliver($ids = null)
    {
        $row = $this->model->find($ids);

        if(!$row)
        {
            $this->error('订单不存在');
        }


        // 处理提交表单
        if($this->request->isPost())
        {
            $params = $this->request->param('row/a');

            // 封装数据
            $data = [
                'id' => $ids,
                'expressid' => $params['expressid'],
                'expresscode' => $params['expresscode'],
                'shipmanid' => $this->auth->id,
                'status' => 2
            ];

            // 定义验证器
            $validate = [
                [
                    'expressid' => 'require',
                    'expresscode' => 'require|unique:order'
                ],
                [
                    'expressid.require' => '配送物流未知',
                    'expresscode.unique' => '配送物流单号已存在，请重新输入',
                    'expresscode.require' => '请输入配送物流单号'
                ]
            ];

            $result = $this->model->validate(...$validate)->isUpdate(true)->save($data);

            if($result === false)
            {
                $this->error($this->model->getError());
            }else{
                $this->success('发货成功');
            }
        }


        // 查询物流公司的数据
        $ExpData = model('Express')->column('id,name');

        $this->assign([
            'ExpData' => $ExpData,
            'row' => $row
        ]);

        return $this->fetch();
    }

    // 软删除
    public function del($ids = null)
    {
        $ids = $ids ?: $this->request->params('ids','','trim');

        $row = $this->model->where('id','in',$ids)->select();

        if(!$row)
        {
            $this->error('请选择需要删除的订单');
        }

        $result = $this->model->destroy($ids);

        if($result === false)
        {
            $this->error('删除失败');
        }else{
            $this->success('删除成功');
        }
    }

    // 回收站
    public function recyclebin()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);

        // 判断是否有Ajax请求
        if($this->request->isAjax())
        {
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }

            [$where, $sort, $order, $offset, $limit] = $this->buildparams();

            $list = $this->model
                ->onlyTrashed()
                ->with(['express','business'])
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = ['total' => $list->total(), 'rows' => $list->items()];

            return json($result);
        }

        return $this->fetch();
    }

    // 还原数据
    public function restore($ids = null)
    {

        $ids = $ids ?: $this->request->params('ids','','trim');

        $row = $this->model->onlyTrashed()->where('id','in',$ids)->select();

        if(!$row)
        {
            $this->error('请选择需要还原的数据');
        }

        $result = $this->model->onlyTrashed()->where('id','in',$ids)->update(['deletetime' => null]);

        if($result === false)
        {
            $this->error('还原失败');
        }else{
            $this->success('还原成功');
        }
    }

    public function destroy($ids = null)
    {
        $ids = $ids ?: $this->request->params('ids','','trim');

        $row = $this->model->onlyTrashed()->where(['id' => ['in',$ids]])->select();

        if(!$row)
        {
            $this->error('请选择需要删除的数据');
        }

        // 开启事务
        Db::startTrans();

        try{

            $OrderProductData = $this->OrderProductModel->where(['orderid' => ['in',$ids]])->column('id');

            $OrderStatus = $this->model->destroy($ids,true);

            if($OrderStatus === FALSE)
            {
                // 定义异常信息
                throw new Exception(__($this->model->getError()));
            }

            $OrderProductStatus = $this->OrderProductModel->destroy($OrderProductData);

            if($OrderProductStatus === FALSE)
            {
                throw new Exception(__($this->OrderProductModel->getError()));
            }

            Db::commit();

        }catch(Exception $e){
            Db::rollback();
            $this->error($e->getMessage());
        }

        $this->success();

    }

    public function refund($ids = null)
    {
        $ids = $ids ?: $this->request->param('ids',0,'trim');

        $row = $this->model->find($ids);

        if(!$row)
        {
            $this->error('订单不存在');
        }

        if($this->request->isPost())
        {
            $params = $this->request->param('row/a');

            if(empty($params['examinereason']) && $params['refund'] == 0)
            {
                $this->error('请填写不同意退货的原因');
            }

            // 同意仅退款
            if($params['refund'] === '1' && $row['status'] === '-1')
            {
                $BusinessModel = model('Business.Business');

                $business = $BusinessModel->find($row['busid']);

                if(!$business)
                {
                    $this->error('用户不存在');
                }

                // 开启事务
                $BusinessModel->startTrans();
                $this->model->startTrans();

                // 更新用户余额
                $BusinessData = [
                    'id' => $business['id'],
                    'money' => bcadd($row['amount'],$business['money'],2)
                ];

                $BusinessStatus = $BusinessModel->isUpdate(true)->save($BusinessData);

                if($BusinessStatus === false)
                {
                    $this->error('更新用户余额失败');
                }

                // 更新订单的状态
                $OrderData = [
                    'id' => $ids,
                    'status' => -4,
                ];

                $OrderStatus = $this->model->isUpdate(true)->save($OrderData);

                if($OrderStatus === false)
                {
                    $BusinessModel->rollback();
                    $this->error('更新订单状态失败');
                }

                if($BusinessStatus === false || $OrderStatus === false)
                {
                    $BusinessModel->rollback();
                    $this->model->rollback();
                    $this->error('同意退款失败');
                }else{
                    $BusinessModel->commit();
                    $this->model->commit();
                    $this->success('同意退款成功');
                }
            }

            // 不同意退货
            if($params['refund'] === '0')
            {
                // 封装数据
                $data = [
                    'id' => $ids,
                    'status' => -5,
                    'examinereason' => $params['examinereason']
                ];

                $result = $this->model->isUpdate(true)->save($data);

                if($result === false)
                {
                    $this->error();
                }else{
                    $this->success();
                }
            }

            // 同意退款退货
            if($params['refund'] === '1' && $row['status'] === '-2')
            {
                // 封装数据
                $data = [
                    'id' => $ids,
                    'status' => -3
                ];

                $result = $this->model->isUpdate(true)->save($data);

                if($result === false)
                {
                    $this->error();
                }else{
                    $this->success();
                }
            }
        }

        $this->assign([
            'row' => $row
        ]);

        return $this->fetch();
    }
}
