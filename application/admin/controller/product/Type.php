<?php

namespace app\admin\controller\product;

use app\common\controller\Backend;
use think\exception\PDOException;
use Exception;
use think\Db;

/**
 * 商品分类管理
 *
 * @icon fa fa-circle-o
 */
class Type extends Backend
{
    /**
     * 快速搜索
    */
    protected $searchFields = ['id','name'];

    /**
     * Type模型对象
     * @var \app\common\model\Product\Type
     */
    protected $model = null;

    /**
     * 初始化
    */
    public function _initialize()
    {
        // 手动
        parent::_initialize();
        
        // $this->model = new \app\common\model\Product\Type;

        // 调用助手函数实例化model
        $this->model = model('Product.Type');
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 商品分类列表
    */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);

        // 判断是否有Ajax请求
        if($this->request->isAjax())
        {
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }

            [$where, $sort, $order, $offset, $limit] = $this->buildparams();

            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = ['total' => $list->total(), 'rows' => $list->items()];

            return json($result);
        }

        // 输出视图
        return $this->view->fetch();
    }

    /**
     * 编辑商品分类
     * 
     * @param String $ids 分类主键
     * 
     * @return String
    */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);

        // 找不到数据返回错误信息
        if(!$row)
        {
            $this->error(__('该分类不存在，请重新选择'));
        }

        // 有接收POST提交
        if($this->request->isPost())
        {
            // 接收参数
            $params = $this->request->post('row/a');

            // 如果参数为空返回错误信息
            if(empty($params))
            {
                $this->error(__('请填写相应的内容'));
            }

            // 查询主键是否存在
            $type = $this->model->find($ids);

            if(!$type)
            {
                $this->error(__('该分类不存在，请重新选择'));
            }

            // 封装更新数组
            $data = [
                'id' => $ids,
                'name' => $params['name'],
                'weigh' => $params['weigh'],
                'thumb' => $params['thumb']
            ];

            $result = $this->model->validate('common/Product/Type')->isUpdate(true)->save($data);

            if($result === false)
            {
                if($data['thumb'] != $type['thumb'])
                {
                    // 左边的/去掉
                    $thumb = ltrim($data['thumb'],'/'); 
                    @is_file($thumb) && @unlink($thumb);
                }
                $this->error(__($this->model->getError()));
            }else{
                // 如果图片名称不一样时说明有更新图片，需要把旧图片删除
                if($data['thumb'] != $type['thumb'])
                {
                    $thumb = ltrim($type['thumb'],'/');
                    @is_file('.'.$type['thumb']) && @unlink('.'.$type['thumb']);
                }
                $this->success();
            }

        }

        // 赋值给模板使用的变量
        $data = [
            'row' => $row
        ];

        return $this->view->fetch('',$data);
    }

    /**
     * 新增商品分类
     * 
     * @return String
    */
    public function add()
    {

        if($this->request->isPost())
        {
            $params = $this->request->param('row/a');

            // 如果参数为空返回错误信息
            if(empty($params))
            {
                $this->error(__('请填写相应的内容'));
            }

            // 封装新增的数据
            $data = [
                'name' => $params['name'],
                'weigh' => $params['weigh'],
                'thumb' => $params['thumb']
            ];

            // 插入数据库
            $result = $this->model->validate('common/Product/Type')->save($data);

            if($result === FALSE)
            {
                $thumb = ltrim($data['thumb'],'/');

                @is_file($thumb) && @unlink($thumb);

                $this->error(__($this->model->getError()));
            }else{
                $this->success();
            }
        }

        return $this->view->fetch();
    }

    /**
     * 删除商品分类
     * @param $ids
     * @return void
    */
    public function del($ids = null)
    {

        $ids = $ids ?: $this->request->param('ids','','trim');

        if(empty($ids))
        {
            $this->error(__('请选择需要删除商品分类'));
        }

        $list = $this->model->where('id','in',$ids)->select();

        $count = 0;

        // 开启事务
        Db::startTrans();

        try {
            foreach ($list as $item) {
                $count += $item->delete();
            }

            // 提交事务
            Db::commit();

        } catch (PDOException|Exception $e) {
            // 事务回滚
            Db::rollback();

            // 获取错误信息
            $this->error($e->getMessage());
        }
        if ($count) {
            // 删除图片
            foreach ($list as $value) {
                $thumb = ltrim($value['thumb'],'/');

                @is_file($thumb) && @unlink($thumb);
            }
            $this->success();
        }
    }
}