<?php

namespace app\admin\controller\depot;

use app\common\controller\Backend;

class Recyclebin extends Backend
{
    public function index()
    {
        return $this->fetch();
    }
}
