<?php

namespace app\admin\controller\depot;

use app\common\controller\Backend;

use think\exception\PDOException;
use Exception;
use think\Db;

/**
 * 退货管理
 *
 * @icon fa fa-circle-o
 */
class Back extends Backend
{

    /**
     * Back模型对象
     * @var \app\admin\model\depot\Back
     */
    protected $model = null;

    /**
     * 是否是关联查询
    */
    protected $relationSearch = false;

    // 订单商品模型
    protected $BackProductModel = null;

    /**
     * 快速搜索
    */
    protected $searchFields = ['id','code','ordercode','createtime','phone'];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\common\model\Depot\back\Back;
        $this->BackProductModel = model('Depot.back.Product');
        $this->view->assign("statusList", $this->model->getStatusList());
        $this->view->assign("statusList", $this->model->getStatusList());
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 首页
    */
    public function index()
    {
        // 设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);

        // 判断是否有Ajax请求
        if($this->request->isAjax())
        {
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }

            [$where, $sort, $order, $offset, $limit] = $this->buildparams();

            $list = $this->model
                ->with(['business'])
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = ['total' => $list->total(), 'rows' => $list->items()];

            return json($result);
        }

        return $this->view->fetch();
    }

    /**
     * 添加退货
    */
    public function add()
    {
        if($this->request->isPost())
        {
            $params = $this->request->param('row/a');

            // 开启事务
            $this->model->startTrans();
            $this->BackProductModel->startTrans();

            

            // 查询订单
            $order = model('Product.order.Order')->where(['code' => $params['ordercode']])->find();

            if(!$order)
            {
                $this->error(__('订单不存在'));
            }

            // 封装退货单数据
            $BackData = [
                'code' => build_code('BP'),
                'ordercode' => $params['ordercode'],
                'busid' => $order['busid'],
                'remark' => $params['remark']
            ];

            $OrderProduct = model('Product.order.Product')->where(['orderid' => $order['id']])->select();

            if(!$OrderProduct)
            {
                $this->error(__('订单商品不存在'));
            }

            $BackData['amount'] = $order['amount'];

            $BackData['status'] = 0;

            $BackData['adminid'] = $this->auth->id;

            // 查询地址
            $address = model('Business.Address')->where(['id' => $params['addrid']])->find();

            if(!$address)
            {
                $this->error(__('请选择联系人以及地址'));
            }

            $BackData['contact'] = $address['consignee'];

            $BackData['phone'] = $address['mobile'];

            $BackData['address'] = $address['address'];

            $BackData['province'] = $address['province'];
            
            $BackData['city'] = $address['city'];

            $BackData['district'] = $address['district'];

            $BackStatus = $this->model->validate('common/Depot/back/Back')->save($BackData);

            if($BackStatus === FALSE)
            {
                $this->error(__($this->model->getError()));
            }

            // 封装退货商品
            $BackProductData = [];

            foreach($OrderProduct as $item)
            {
                $BackProductData[] = [
                    'backid' => $this->model->id,
                    'proid' => $item['proid'],
                    'nums' => $item['pronum'],
                    'price' => $item['price'],
                    'total' => $item['total']
                ];
            }

            $BackProductStatus = $this->BackProductModel->validate('common/Depot/back/BackProduct')->saveAll($BackProductData);

            if($BackProductStatus === FALSE)
            {
                $this->model->rollback();
                $this->error(__($this->BackProductModel->getError()));
            }
            
            if($BackProductStatus === FALSE || $BackStatus === FALSE)
            {
                $this->model->rollback();
                $this->BackProductModel->rollback();
                $this->error(__("添加失败"));
            }else{
                $this->model->commit();
                $this->BackProductModel->commit();
                $this->success();
            }
        }

        return $this->view->fetch();
    }


    /**
     * 编辑退货
    */
    public function edit($ids = null)
    {
        $row = $this->model->with(['business'])->find($ids);

        if(!$row)
        {
            $this->error(__('该退货单不存在'));
        }

        if($this->request->isPost())
        {
            $params = $this->request->param('row/a');

            // 开启事务
            $this->model->startTrans();
            $this->BackProductModel->startTrans();

            // 查询订单
            $order = model('Product.order.Order')->where(['code' => $params['ordercode']])->find();

            if(!$order)
            {
                $this->error(__('订单不存在'));
            }

            $OrderProduct = model('Product.order.Product')->where(['orderid' => $order['id']])->select();

            if(!$OrderProduct)
            {
                $this->error(__('订单商品不存在'));
            }

            // 封装退货单数据
            $BackData = [
                'id' => $ids,
                'ordercode' => $params['ordercode'],
                'busid' => $order['busid'],
                'remark' => $params['remark'],
                'amount' => $order['amount'],
                'status' => $row['status'],
                'adminid' => $this->auth->id
            ];

            // 查询地址
            $address = model('Business.Address')->where(['id' => $params['addrid']])->find();

            if(!$address)
            {
                $this->error(__('请选择联系人以及地址'));
            }

            $BackData['contact'] = $address['consignee'];

            $BackData['phone'] = $address['mobile'];

            $BackData['address'] = $address['address'];

            $BackData['province'] = $address['province'];
            
            $BackData['city'] = $address['city'];

            $BackData['district'] = $address['district'];

            // 更新数据库
            $BackStatus = $this->model->isUpdate(true)->save($BackData);

            if($BackStatus === FALSE)
            {
                $this->error(__($this->model->getError()));
            }

            // 默认退货订单状态
            $BackProductStatus = TRUE;

            // 判断是否订单号有更新
            if($params['ordercode'] != $row['ordercode'])
            {
                // 封装退货商品
                $BackProductData = [];

                foreach($OrderProduct as $item)
                {
                    $BackProductData[] = [
                        'backid' => $this->model->id,
                        'proid' => $item['proid'],
                        'nums' => $item['pronum'],
                        'price' => $item['price'],
                        'total' => $item['total']
                    ];
                }

                $BackProductStatus = $this->BackProductModel->validate('common/Depot/back/BackProduct')->saveAll($BackProductData);
            }

            if($BackProductStatus === FALSE)
            {
                $this->model->rollback();
                $this->error(__($this->BackProductModel->getError()));
            }
            
            if($BackProductStatus === FALSE || $BackStatus === FALSE)
            {
                $this->model->rollback();
                $this->BackProductModel->rollback();
                $this->error(__("修改失败"));
            }else{
                $this->model->commit();
                $this->BackProductModel->commit();
                $this->success();
            }
            
        }

        // 查询地址数据
        $AddressWhere = [
            'consignee' => $row['contact'],
            'mobile' => $row['phone'],
            'address' => $row['address'],
            'province' => $row['province'],
            'city' => $row['city'],
            'district' => $row['district'],
            'busid' => $row['busid']
        ];

        $addrid = model('Business.Address')->where($AddressWhere)->value('id');

        $row['addrid'] = $addrid;

        $BackProductList = $this->BackProductModel->with(['products'])->where(['backid' => $row['id']])->select();

        $AddressData = model('Business.Address')->with(['provinces','citys','districts'])->where(['busid' => $row['busid']])->select();

        // 封装下拉的数据
        $AddressList = [];

        // 遍历数据
        foreach ($AddressData as $key => $item) {
            $AddressList[$item['id']] = "联系人：{$item['consignee']} 联系方式：{$item['mobile']} 地址：{$item['provinces']['name']}-{$item['citys']['name']}-{$item['districts']['name']} {$item['address']}";
        }

        $this->assignconfig('back', ['BackProductList' => $BackProductList]);

        $data = [
            'row' => $row,
            'AddressList' => $AddressList,
        ];

        return $this->view->fetch('',$data);
    }

    /**
     * 软删除
    */
    public function del($ids = null)
    {
        if($this->request->isAjax())
        {
            $row = $this->model->where(['id' => ['in',$ids]])->select();

            if(empty($row))
            {
                $this->error(__('请选择需要删除退货单'));
            }

            $result = $this->model->destroy($ids);

            if($result === FALSE)
            {
                $this->error(__('删除退货单失败'));
            }else {
                $this->success();
            }
        }
    }

    /**
     * 回收站
    */
    public function recyclebin()
    {
        // 设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);

        // 判断是否有Ajax请求
        if($this->request->isAjax())
        {
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }

            [$where, $sort, $order, $offset, $limit] = $this->buildparams();

            $list = $this->model
                ->onlyTrashed()
                ->with(['business'])
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = ['total' => $list->total(), 'rows' => $list->items()];

            return json($result);
        }

        return $this->view->fetch();
    }

    /**
     * 还原数据
    */
    public function restore($ids = null)
    {
        $row = $this->model->onlyTrashed()->where(['id' => ['in',$ids]])->select();

        if(!$row)
        {
            $this->error(__('请选择需要还原记录'));
        }

        $result = $this->model->onlyTrashed()->where(['id' => ['in',$ids]])->update(['deletetime' => null]);

        if($result != true)
        {
            $this->error(__('还原失败'));
        }else{
            $this->success();
        }
    }

    /**
     * 真实删除
    */
    public function destroy($ids = null)
    {
        if($this->request->isAjax())
        {
            Db::startTrans();

            try {
                $row = $this->model->onlyTrashed()->where(['id' => ['in',$ids]])->select();

                if(empty($row))
                {
                    throw new Exception(__("请选择需要删除退货单"));
                }

                $BackProduct = $this->BackProductModel->where(['Backid' => ['in',$ids]])->column('id');

                $BackStatus = $this->model->destroy($ids,true);

                if($BackStatus === FALSE)
                {
                    throw new Exception(__($this->model->getError()));
                }

                $ProductStatus = $this->BackProductModel->destroy($BackProduct);

                if($ProductStatus === FALSE)
                {
                    throw new Exception(__($this->BackProductModel->getError()));
                }

                foreach($row as $item)
                {
                    if(!empty($item['thumbs']))
                    {

                        $thumbs = explode(',',$item['thumbs']);

                        foreach($thumbs as $val)
                        {
                            @is_file('.'.$val) && @unlink('.'.$val);
                        }
                    }
                }

                Db::commit();
            } catch (PDOException|Exception $e) {
                Db::rollback();
                $this->error(__($e->getMessage()));
            }

            $this->success();
        }
    }

    /**
     * 退货单详情
    */
    public function detail($ids = null)
    {
        $row = $this->model->with(['business'])->find($ids);

        if(!$row)
        {
            $this->error(__('退货单不存在'));
        }

        $BackProductList = $this->BackProductModel->with(['products'])->where(['backid' => $ids])->select();

        $this->view->assign([
            'row' => $row,
            'BackProductList' => $BackProductList
        ]);

        return $this->view->fetch();
    }

    /**
     * 审核通过
    */
    public function process()
    {
        if($this->request->isAjax())
        {
            $ids = $this->request->param('ids');

            $row = $this->model->find($ids);

            if(!$row)
            {
                $this->error(__('退货单不存在'));
            }

            // 封装更新的数据
            $data = [
                'id' => $ids,
                'status' => 1,
                'reviewerid' => $this->auth->id
            ];

            $result = $this->model->isUpdate(true)->save($data);

            if($result === FALSE)
            {
                $this->error('审核通过失败');
            }else {
                $this->success();
            }
        }
    }

    /**
     * 撤回审核
    */
    public function cancel()
    {
        if($this->request->isAjax())
        {
            $ids = $this->request->param('ids');

            $row = $this->model->find($ids);

            if(!$row)
            {
                $this->error(__('退货单不存在'));
            }

            // 封装更新的数据
            $data = [
                'id' => $ids,
                'status' => 0,
                'reviewerid' => $this->auth->id
            ];

            $result = $this->model->isUpdate(true)->save($data);

            if($result === FALSE)
            {
                $this->error('撤回审核失败');
            }else {
                $this->success();
            }
        }
    }

    /**
     * 确认入库
    */
    public function storage()
    {
        if($this->request->isAjax())
        {
            $ids = $this->request->param('ids');

            $row = $this->model->find($ids);

            if(!$row)
            {
                $this->error(__('退货单不存在'));
            }

            $BackProductList = $this->BackProductModel->where(['backid' => $ids])->select();

            if(!$BackProductList)
            {
                $this->error(__('退货商品不存在'));
            }

            model('Depot.storage.Storage')->startTrans();
            model('Depot.storage.Product')->startTrans();
            $this->model->startTrans();

            // 封装入库数据
            $StorageData =[
                'code' => build_code('SU'), // 订单前缀可以自定义\
                'type' => 2,
                'amount' => $row['amount'],
                'status' => 0
            ];

            $StorageStatus = model('Depot.storage.Storage')->validate('common/Depot/storage/Storage.back')->save($StorageData);

            if($StorageStatus === FALSE)
            {
                $this->error(model('Depot.storage.Storage')->getError());
                exit;
            }

            // 存放封装好的商品数据
            $ProductData = [];

            foreach($BackProductList as $item)
            {
                $ProductData[] = [
                    'storageid' => model('Depot.storage.Storage')->id,
                    'proid' => $item['proid'],
                    'nums' => $item['nums'],
                    'price' => $item['price'],
                    'total' => $item['total'],
                ];
            }

            // 验证数据
            $ProductStatus = model('Depot.storage.Product')->validate('common/Depot/storage/Product')->saveAll($ProductData);

            if($ProductStatus === FALSE)
            {
                model('Depot.storage.Storage')->rollback();
                $this->error(model('Depot.storage.Product')->getError());
            }

            // 更新退货单的数据
            $BackData = [
                'id' => $row['id'],
                'status' => 3,
                'stromanid' => $this->auth->id,
                'storageid' => model('Depot.storage.Storage')->id
            ];

            $BackStatus = $this->model->isUpdate(true)->save($BackData);

            if($BackStatus === FALSE)
            {
                model('Depot.storage.Storage')->rollback();
                model('Depot.storage.Product')->rollback();
                $this->error($this->model->getError());
            }

            // 大判断
            if($ProductStatus === FALSE || $StorageStatus === FALSE || $BackStatus === FALSE)
            {
                model('Depot.storage.Storage')->rollback();
                model('Depot.storage.Product')->rollback();
                $this->model->rollback();
                $this->error(__('入库失败'));
            }else {
                model('Depot.storage.Storage')->commit();
                model('Depot.storage.Product')->commit();
                $this->model->commit();
                $this->success();
            }

        }
    }

    /**
     * 审核不通过
    */
    public function fail()
    {

        $ids = $this->request->param('ids');

        $row = $this->model->find($ids);

        if(!$row)
        {
            $this->error(__('退货单不存在'));
        }

        if($this->request->isPost())
        {
            $params = $this->request->param('row/a');

            if(empty($params['reason']))
            {
                $this->error('请填写作废理由');
            }

            // 封装更新的数据
            $data = [
                'id' => $ids,
                'status' => '-1',
                'reviewerid' => $this->auth->id,
                'reason' => $params['reason']
            ];

            $result = $this->model->isUpdate(true)->save($data);

            if($result === FALSE)
            {
                $this->error('审核不通过失败');
            }else {
                $this->success();
            }

            halt($params);
        }

        return $this->view->fetch();
    }

    /**
     * 确认收货
    */
    public function receipt()
    {
        if($this->request->isAjax())
        {
            $ids = $this->request->param('ids');

            $row = $this->model->find($ids);

            if(!$row)
            {
                $this->error(__('退货单不存在'));
            }

            // 实例化模型
            $OrderModel = model('Product.order.Order');
            $BusinessModel = model('Business.Business');

            // 查询订单
            $order = $OrderModel->where(['code' => $row['ordercode']])->find();

            if(!$order)
            {
                $this->error('商品订单不存在');
            }

            $business = $BusinessModel->find($order['busid']);

            if(!$business)
            {
                $this->error('用户不存在');
            }

            // 开启事务
            $BusinessModel->startTrans();
            $this->model->startTrans();
            $OrderModel->startTrans();
            
            // 封装更新退货单数据
            $data = [
                'id' => $ids,
                'status' => 2
            ];

            $BackStatus = $this->model->isUpdate(true)->save($data);

            if($BackStatus === FALSE)
            {
                $this->error('确认收货失败');
            }

            // 更新用户的余额
            $BusinessData = [
                'id' => $business['id'],
                'money' => bcadd($order['amount'],$business['money'],2)
            ];

            $BusinessStatus = $BusinessModel->isUpdate(true)->save($BusinessData);

            if($BusinessStatus === false)
            {
                $this->model->rollback();
                $this->error('更新用户余额失败');
            }

            // 更新订单的状态
            $OrderData = [
                'id' => $ids,
                'status' => -4,
            ];

            $OrderStatus = $OrderModel->isUpdate(true)->save($OrderData);

            if($OrderStatus === false)
            {
                $this->model->rollback();
                $BusinessModel->rollback();
                $this->error('更新订单状态失败');
            }

            if($BackStatus === false || $BusinessStatus === false || $OrderStatus === false)
            {
                $this->model->rollback();
                $BusinessModel->rollback();
                $OrderModel->rollback();
                $this->error();
            }else{
                $this->model->commit();
                $BusinessModel->commit();
                $OrderModel->commit();
                $this->success();
            }
        }
    }

    /**
     * 查询订单
    */
    public function order()
    {
        if($this->request->isAjax())
        {
            $code = $this->request->param('code');

            $order = model('Product.order.Order')->where(['code' => $code])->find();

            if(!$order)
            {
                $this->error(__('订单号不存在,请重新填写'));
            }

            $OrderProduct = model('Product.order.Product')->with(['products'])->where(['orderid' => $order['id']])->select();

            $AddressList = model('Business.Address')->with(['provinces','citys','districts'])->where(['busid' => $order['busid']])->select();

            $this->success('查询成功',null,['OrderProduct' => $OrderProduct,'AddressList' => $AddressList,'order' => $order]);
        }
    }
}
