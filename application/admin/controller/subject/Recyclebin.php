<?php

namespace app\admin\controller\subject;

use app\common\controller\Backend;

/**
 * 回收站控制器
 */

class Recyclebin extends Backend
{
    protected $model = null;
    //当前无须登录方法
    protected $noNeedLogin = [];

    //无需鉴权的方法,但需要登录
    protected $noNeedRight = [];
    //设置关联查询
    protected $relationSearch = true;

    public function __construct()
    {
        parent::__construct();

        $this->model = model('Subject.Subject');
    }

    public function index()
    {

        return $this->view->fetch();
    }

    // 课程回收站
    public function recy_sub()
    {
        //接收请求，查询数据
        // 判断是否有ajax请求

        //将请求当中所有的参数去除html标签，去掉两边空白
        $this->request->filter(['strip_tags', 'trim']);

        if ($this->request->isAjax()) {
            //获取表格所提交过来筛选和排序的一些参数
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            //表格需要两个返回值参数 总条数，分页的数据
            // onlyTrashed 仅需要查询软删除的数据
            $total = $this->model
                ->onlyTrashed()
                ->with(['category'])
                ->where($where)
                ->order($sort, $order)
                ->count(); //返回查询的总数

            //返回多条数据，是二维数组结构
            $list = $this->model
                ->onlyTrashed()
                ->with(['category'])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            //组装数据
            $result = ['total' => $total, 'rows' => $list];

            //返回json数据给ajax结果
            return json($result);
        }

        return $this->view->fetch();
    }

    // 课程订单回收站
    public function recy_order()
    {

        //将控制器和模型关联
        $this->model = model('Subject.Order');
        //将请求当中所有的参数去除html标签，去掉两边空白
        $this->request->filter(['strip_tags', 'trim']);

        if ($this->request->isAjax()) {
            //获取表格所提交过来筛选和排序的一些参数
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            //表格需要两个返回值参数 总条数，分页的数据
            $total = $this->model
                ->onlyTrashed()
                ->with(['business', 'subject'])
                ->where($where)
                ->order($sort, $order)
                ->count(); //返回查询的总数

            //返回多条数据，是二维数组结构
            $list = $this->model
                ->onlyTrashed()
                ->with(['business', 'subject'])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            //组装数据
            $result = ['total' => $total, 'rows' => $list];

            //返回json数据给ajax结果
            return json($result);
        }

        return $this->view->fetch();
    }


}
