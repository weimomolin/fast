<?php

namespace app\admin\controller\subject;

//引入进来基类控制器
use app\common\controller\Backend;

/**
 * 课程详情管理控制器
 */
class Info extends Backend{
    
    //设置关联查询
    protected $relationSearch = true;

    //当前模型
    protected $model = null;

    //当前无须登录方法
    protected $noNeedLogin = [];

    //无需鉴权的方法,但需要登录
    protected $noNeedRight = [];

    //构造函数
    public function __construct()
    {
        parent::__construct();

        //将控制器和模型关联
        $this->model = model('Subject.Subject');
        $this->SubjectModel = model('Subject.Subject');
        $this->OrderModel = model('Subject.Order');
        $this->CommentModel = model('Subject.Comment');
    }
    
    public function index($ids = NULL)
    {
        $row = $this->model->find($ids);

        if(!$row)
        {
            $this->error('课程不存在');
            exit;
        }

        return $this->view->fetch();
    }

    //课程订单
    public function order($ids = NULL)
    {
        //修改当前的验证模型
        $this->model = $this->OrderModel;


        //先判断课程是否存在
        $subject = $this->SubjectModel->find($ids);

        if(!$subject)
        {
            $this->error(__('No Results were found'));
            exit;
        }

        //查询课程订单
        $this->request->filter(['strip_tags', 'trim']);

        if($this->request->isAjax())
        {
            //获取表格所提交过来筛选和排序的一些参数
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            //表格需要两个返回值参数 总条数，分页的数据
            $total = $this->model
                ->with(['business'])
                ->where('subid',$ids)
                ->where($where)
                ->order($sort, $order)
                ->count(); //返回查询的总数
            
            //返回多条数据，是二维数组结构
            $list = $this->model
                ->with(['business'])
                ->where('subid',$ids)
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            // 打印多条数据
            // var_dump(collection($list)->toArray());
            // exit;

            //打印sql语句
            // echo $this->model->getLastSql();
            // exit;

            //组装数据
            $result = ['total' => $total, 'rows' => $list];

            //返回json数据给ajax结果
            return json($result);
        }
    }

    //课程评论
    public function comment($ids = NULL)
    {
        //修改当前的验证模型
        $this->model = $this->CommentModel;


        //先判断课程是否存在
        $subject = $this->SubjectModel->find($ids);

        if(!$subject)
        {
            $this->error(__('No Results were found'));
            exit;
        }

        //查询课程订单
        $this->request->filter(['strip_tags', 'trim']);

        if($this->request->isAjax())
        {
            //获取表格所提交过来筛选和排序的一些参数
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            //表格需要两个返回值参数 总条数，分页的数据
            $total = $this->model
                ->with(['business'])
                ->where('subid',$ids)
                ->where($where)
                ->order($sort, $order)
                ->count(); //返回查询的总数
            
            //返回多条数据，是二维数组结构
            $list = $this->model
                ->with(['business'])
                ->where('subid',$ids)
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            // 打印多条数据
            // var_dump(collection($list)->toArray());
            // exit;

            //打印sql语句
            // echo $this->model->getLastSql();
            // exit;

            //组装数据
            $result = ['total' => $total, 'rows' => $list];

            //返回json数据给ajax结果
            return json($result);
        }
    }
}

