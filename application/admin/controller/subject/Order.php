<?php

namespace app\admin\controller\subject;

//引入进来基类控制器
use app\common\controller\Backend;

/**
 * 课程订单控制器
 */
class Order extends Backend
{

    //设置关联查询
    protected $relationSearch = true;

    //当前模型
    protected $model = null;

    //当前无须登录方法
    protected $noNeedLogin = [];

    //无需鉴权的方法,但需要登录
    protected $noNeedRight = [];

    //构造函数
    public function __construct()
    {
        parent::__construct();

        //将控制器和模型关联
        $this->model = model('Subject.Order');
    }

    // 课程订单列表
    public function index()
    {
        // 前端js表格发送的ajax请求

        //将请求当中所有的参数去除html标签，去掉两边空白
        $this->request->filter(['strip_tags', 'trim']);

        // 判断请求
        if ($this->request->isAjax()) {
            // 获取js表格提交过来的筛选、排序参数
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            //表格需要两个返回值参数 总条数，分页的数据
            $total = $this->model
                ->with(['business', 'subject'])
                ->where($where)
                ->order($sort, $order)
                ->count(); //返回查询的总数

            //返回多条数据，是二维数组结构
            $list = $this->model
                ->with(['business', 'subject'])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            //组装数据
            $result = ['total' => $total, 'rows' => $list];

            //返回json数据给ajax结果
            return json($result);
        };

        return $this->view->fetch();
    }

    //软删除
    public function del($ids = NULL)
    {
        $list = $this->model->select($ids);

        //没有数据
        if (!$list) {
            $this->error(__('No Results were found'));
            exit;
        }
        //软删除
        $result = $this->model->destroy($ids);

        if ($result === FALSE) {
            $this->error('删除失败');
            exit;
        } else {
            $this->success('删除成功');
            exit;
        }
    }

    // --------------- 回收站 ---------------
    //恢复
    public function restore($ids = NULL)
    {
        //根据id判断数据是否存在
        $rows = $this->model->withTrashed()->whereIn('id', $ids)->select();

        if (!$rows) {
            $this->error(__('No Results were found'));
            exit;
        }

        $result = $this->model->withTrashed()->whereIn('id', $ids)->update(['deletetime' => NULL]);

        if ($result === FALSE) {
            $this->error('恢复数据失败');
            exit;
        } else {
            $this->success('恢复数据成功');
            exit;
        }
    }

    //销毁
    public function destroy($ids = NULL)
    {
       //根据id判断数据是否存在
       $rows = $this->model->withTrashed()->whereIn('id', $ids)->select();

        //没有数据
        if (!$rows) {
            $this->error(__('No Results were found'));
            exit;
        }

        //删除
        $result = $this->model->withTrashed()->whereIn('id', $ids)->delete(true);

        if ($result === FALSE) {
            $this->error('删除失败');
            exit;
        } else {
            $this->success('删除成功');
            exit;
        }
    }
}
