<?php

namespace app\admin\controller\subject;

//引入进来基类控制器
use app\common\controller\Backend;

/**
 * 课程章节管理控制器
 */
class Chapter extends Backend
{

    //设置关联查询
    protected $relationSearch = true;

    //当前模型
    protected $model = null;

    //当前无须登录方法
    protected $noNeedLogin = [];

    //无需鉴权的方法,但需要登录
    protected $noNeedRight = [];

    //构造函数
    public function __construct()
    {
        parent::__construct();

        // 将控制器和模型关联
        $this->model = model('Subject.Chapter');
    }

    // 章节列表
    public function index($ids = NULL)
    {
        //先判断章节是否存在
        $row = $this->model->where('subid', $ids)->select();

        if ($row === null) {
            $this->error(__('No Results were found'));
            exit;
        }

        //过滤 检查是否为空 去掉两边空白
        $this->request->filter(['strip_tags', 'trim']);

        if ($this->request->isAjax()) {

            //获取表格所提交过来筛选和排序的一些参数
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            //表格需要两个返回值参数 总条数，分页的数据
            $total = $this->model
                ->with(['subject'])
                ->where('subid', $ids)
                ->where($where)
                ->order($sort, $order)
                ->count(); //返回查询的总数

            //返回多条数据，是二维数组结构
            $list = $this->model
                ->with(['subject'])
                ->where('subid', $ids)
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            // 打印多条数据
            // var_dump(collection($list)->toArray());
            // exit;

            //打印sql语句
            // echo $this->model->getLastSql();
            // exit;

            //组装数据
            $result = ['total' => $total, 'rows' => $list];

            //返回json数据给ajax结果
            return json($result);
        }

        return $this->view->fetch();
    }

    // 添加章节
    public function add($ids = NULL)
    {
        if ($this->request->isPost()) {
            //接收row前缀的数据，并接收为数组类型
            $params = $this->request->param('row/a');
            //组装数据
            $data = [
                'title' => $params['title'],
                'url' => $params['videoUrl'],
                'subid' => $ids,
            ];
            //插入到数据库中
            $result = $this->model->save($data);

            if ($result === FALSE) {
                $this->error($this->model->getError());
                exit;
            } else {
                $this->success('添加课程成功');
                exit;
            }
        }

        return $this->view->fetch();
    }

    // 编辑章节
    public function edit($ids = NULL)
    {
        //根据id要判断数据是否存在
        $row = $this->model->find($ids);

        //数据丢失，记录不存在
        if (!$row) {
            $this->error(__('No Results were found'));
            exit;
        }

        //判断是否有post过来请求
        if ($this->request->isPost()) {
            //接收row前缀请求参数，并返回一个数组类型
            $params = $this->request->param('row/a');

            //组装数据
            $data = [
                'title' => $params['title'],
                'url' => $params['videoUrl'],
                'subid' => $row['subid'],
            ];

            //直接去更新
            $result = $this->model->where(['id' => $ids])->isUpdate(true)->save($data);

            if ($result === FALSE) {
                $this->error($this->model->getError());
                exit;
            }

            //判断是否有上传新视频 
            if ($data['url'] != $row['url']) {
                //不相等就说明有换图片了，就删除掉旧图片
                is_file("." . $row['url']) && @unlink("." . $row['url']);
            }

            $this->success('编辑课程成功');
            exit;
        }

        // 有数据要赋值到模板中去
        $this->assign('row', $row);

        //模板渲染
        return $this->view->fetch();
    }

    // 删除
    public function delt($ids = NULL)
    {
        $list = $this->model->select($ids);

        //没有数据
        if (!$list) {
            $this->error(__('No Results were found'));
            exit;
        }

        //删除
        $result = $this->model->destroy($ids);

        if ($result === FALSE) {
            $this->error('删除失败');
            exit;
        } else {
            // 删除文件
            is_file("." . $list['url']) && @unlink("." . $list['url']);
            $this->success('删除成功');
            exit;
        }
    }
}
