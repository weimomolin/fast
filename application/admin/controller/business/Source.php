<?php

namespace app\admin\controller\business;

//引入进来基类控制器
use app\common\controller\Backend;

/**
 * 客户来源控制器
 */
class Source extends Backend
{
    //当前模型
    protected $model = null;

    //当前无须登录方法
    protected $noNeedLogin = [];

    //无需鉴权的方法,但需要登录
    protected $noNeedRight = [];

    //构造函数
    public function __construct()
    {
        parent::__construct();

        // 将控制器和模型关联
        $this->model = model('Business.Source');
    }

    // 客户来源列表
    public function index()
    {
        //将请求当中所有的参数去除html标签，去掉两边空白
        $this->request->filter(['strip_tags', 'trim']);
        // 判断是否有ajax请求
        if ($this->request->isAjax()) {
            //获取表格所提交过来筛选和排序的一些参数
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            //表格需要两个返回值参数 总条数，分页的数据
            $total = $this->model
                ->where($where)
                ->order($sort, $order)
                ->count(); //返回查询的总数

            //返回多条数据，是二维数组结构
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            //组装数据
            $result = ['total' => $total, 'rows' => $list];

            //返回json数据给ajax结果
            return json($result);
        }
        return $this->view->fetch();
    }

    // 添加客户来源
    public function add()
    {
        if ($this->request->isPost()) {
            //接收row前缀的数据，并接收为数组类型
            $params = $this->request->param('row/a');

            //组装数据
            $data = [
                'name' => $params['name'],
            ];

            //插入到数据库中
            $result = $this->model->save($data);

            if ($result === FALSE) {
                $this->error($this->model->getError());
                exit;
            } else {
                $this->success('添加客户来源成功');
                exit;
            }
        }
        return $this->view->fetch();
    }

    // 编辑客户来源
    public function edit($ids = NULL)
    {
        //根据id要判断数据是否存在
        $row = $this->model->find($ids);

        //数据丢失，记录不存在
        if (!$row) {
            $this->error(__('No Results were found'));
            exit;
        }

        //判断是否有post过来请求
        if ($this->request->isPost()) {
            //接收row前缀请求参数，并返回一个数组类型
            $params = $this->request->param('row/a');

            //组装数据
            $data = [
                'id' => $ids,
                'name' => $params['name'],
            ];

            //直接去更新
            $result = $this->model->isUpdate(true)->save($data);

            if ($result === FALSE) {
                $this->error($this->model->getError());
                exit;
            }

            $this->success('编辑客户来源成功');
            exit;
        }

        // 有数据要赋值到模板中去
        $this->assign('row', $row);

        //模板渲染
        return $this->view->fetch();
    }

    //删除
    public function del($ids = NULL)
    {
        // 根据id判断数据是否存在
        $rows = $this->model->whereIn('id', $ids)->select();

        if (!$rows) {
            $this->error(__('No Results were found'));
            exit;
        }


        // 删除 
        $result = $this->model->whereIn('id', $ids)->delete(true);

        if ($result  === FALSE) {
            $this->error('删除客户来源失败');
            exit;
        } else {
            $this->success('删除客户来源成功');
            exit;
        }
    }
}
