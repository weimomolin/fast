<?php

return [
    // 客户私海
    'Nickname'  => '客户名称',
    'Mobile' => '手机号码',
    'Email'  => '邮箱',
    'Money'  => '账户余额',
    'SourceName'  => '客户来源',
    'Gender'  => '性别',
    'Deal'  => '成交状态',
    'Auth'  => '认证状态',
    'AdminName' => '申请人',
    'Bnickname'  => '客户名称',
    'SName'  => '客户来源',
    'BsexText'  => '性别',
    'BdealText'  => '成交状态',
    'Vcontent' => '回访内容',
    'Screatetime' => '最后一次回访时间',
    'Ausername' => '申请人',
    'Bmoney' => '账户余额',
    'AuthStatus' => '认证状态'
];
