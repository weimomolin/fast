<?php

return [
    // 客户公海
    'Nickname'  => '客户名称',
    'SourceName'  => '客户来源',
    'Gender'  => '性别',
    'Deal'  => '成交状态',
    'Auth'  => '认证状态',
   // 分配客户
   'RecoveryTitle' => '请选择人员',
   'NicknameList'  => '需分配客户名称',

];
