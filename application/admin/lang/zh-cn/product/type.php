<?php

return [
    'Id'    => '主键',
    'Name'  => '商品分类名称',
    'Thumb' => '商品分类图片',
    'Weigh' => '权重'
];
