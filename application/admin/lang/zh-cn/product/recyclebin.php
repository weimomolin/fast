<?php

return [
    'Id'         => '主键',
    'Name'       => '商品名称',
    'Status'     => '商品状态',
    'Stock'      => '商品库存',
    'Typeid'     => '商品分类',
    'Unitid'     => '商品单位',
    'Flag'       => '标签',

    'Code'           => '订单号',
    'Busid'          => '客户',
    'Amount'         => '订单金额',
    'Remark'         => '订单备注',
    'Expressid'      => '物流公司',
    'Expresscode'    => '物流单号',

    'BusinessNcikName'      => '客户',
    'ProductName'      => '租用商品',
    'Rent'       => '押金',
    'Price'      => '订单价格',
    'Endtime'    => '租用结束时间',
    'ExpName'      => '配送物流',
    'Expcode'    => '配送单号',
    'BusexpName'   => '归还配送物流',
    'Busexpcode' => '归还配送单号',
];