<?php

return [
    'Id'         => '主键',
    'Name'       => '商品名称',
    'Prop'       => '商品属性',
    'Content'    => '商品描述',
    'Thumbs'     => '商品图集',
    'Status'     => '商品状态',
    'Stock'      => '商品库存',
    'Typeid'     => '商品分类',
    'Unitid'     => '商品单位',
    'Createtime' => '上市时间',
    'Flag'       => '标签',
    'Price'      => '商品价格',
    'RentPrice'  => '商品日租金',
    'rent'       => '商品押金',
    'RentStatus' => '商品租赁状态'
];
