<?php

return [
    'Id'         => '主键',
    'Code'       => '单据编号',
    'Supplierid' => '供应商',
    'Type'       => '入库类型',
    'Amount'     => '总价',
    'Screatetime' => '制单日期',
    'Remark'     => '备注',
    'Status'     => '状态',

    'Code'        => '单据号',
    'Ordercode'   => '订单号',
    'Busid'       => '客户',
    'Contact'     => '联系人',
    'Phone'       => '联系电话',
];