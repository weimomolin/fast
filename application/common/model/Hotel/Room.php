<?php

namespace app\common\model\Hotel;

use think\Model;

class Room extends Model
{
    // 表名
    protected $name = 'hotel_room';

    // 追加属性
    protected $append = [
        'thumb_text',
        'flag_text'
    ];

    public function getFlagTextAttr($value, $data)
    {
        $flag = isset($data['flag']) ? trim($data['flag']) : '';

        $list = explode(',', $flag);

        return $list;
    }

    public function getThumbTextAttr($value, $data)
    {
        //获取到cdn的地址
        $cdnurl = config('site.cdnurl') ? config('site.cdnurl') : '';
        $cdnurl = trim($cdnurl, '/');
        
        $thumb = isset($data['thumb']) ? $data['thumb'] : '';

        //如果为空就给一个默认图片地址
        if(empty($thumb) || !@is_file(".".$thumb))
        {
            $thumb = "/assets/img/coupon.jpg";
        }

        return $cdnurl.$thumb;
    }
}
