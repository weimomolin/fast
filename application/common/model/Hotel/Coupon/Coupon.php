<?php

namespace app\common\model\Hotel\Coupon;

use think\Model;

class Coupon extends Model
{
    
    // 表名
    protected $name = 'hotel_coupon';

    // 追加属性
    protected $append = [
        'status_text',
        'thumb_text',
        'createtime_text',
        'endtime_text'
    ];

    // 订单状态数据
    public function statuslist()
    {
        return [
            '0' => __('结束活动'),
            '1' => __('正在活动中'),
        ];
    }

    // 订单状态的获取器
    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->statuslist();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getThumbTextAttr($value, $data)
    {
        //获取到cdn的地址
        $cdnurl = config('site.cdnurl') ? config('site.cdnurl') : '';
        $cdnurl = trim($cdnurl, '/');
        
        $thumb = isset($data['thumb']) ? $data['thumb'] : '';

        //如果为空就给一个默认图片地址
        if(empty($thumb) || !@is_file(".".$thumb))
        {
            $thumb = "/assets/img/coupon.jpg";
        }

        return $cdnurl.$thumb;
    }

    //时间戳
    public function getCreatetimeTextAttr($value, $data)
    {
        $createtime = $data['createtime'];
        
        if(empty($createtime))
        {
            return '';
        }

        return date("Y-m-d", $createtime);
    }

    //时间戳
    public function getEndtimeTextAttr($value, $data)
    {
        $endtime = $data['endtime'];
        
        if(empty($endtime))
        {
            return '';
        }

        return date("Y-m-d", $endtime);
    }
}
