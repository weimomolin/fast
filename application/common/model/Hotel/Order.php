<?php

namespace app\common\model\Hotel;

use think\Model;

class Order extends Model
{
    // 表名
    protected $name = 'hotel_order';

    // 追加属性
    protected $append = [
        'status_text',
    ];

    // 订单状态数据
    public function statuslist()
    {
        return [
            '0' => __('未支付'),
            '1' => __('已支付'),
            '2' => __('已入住'),
            '3' => __('已退房'),
            '4' => __('已评价'),
            '-1' => __('申请退款'),
            '-2' => __('审核通过'),
            '-3' => __('审核不通过'),
        ];
    }

    // 订单状态的获取器
    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->statuslist();
        return isset($list[$value]) ? $list[$value] : '';
    }
}
