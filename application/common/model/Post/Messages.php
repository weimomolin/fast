<?php

namespace app\common\model\Post;

use think\Model;

class Messages extends Model
{
    //标志当前模型操作的是哪张表
    protected $name = "messages";

    // 收信人
    public function recipient()
    {
        return $this->belongsTo('app\common\model\Business\Business', 'recid', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    // 发信人
    public function sender()
    {
        return $this->belongsTo('app\common\model\Business\Business', 'senid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
