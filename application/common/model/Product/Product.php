<?php

namespace app\common\model\Product;

use think\Model;
use traits\model\SoftDelete;

class Product extends Model
{
    // 软删除
    use SoftDelete;

    // 表名
    protected $name = 'product';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'flag_text',
        'status_text',
        'unit_text',
        'thumb_text',
        'thumbs_text',
    ];

    public function flaglist()
    {
        return ['1' => __('新品'), '2' => __('热销'), '3' => __('推荐')];
    }

    public function statuslist()
    {
        return ['1' => __('上架'), '0' => __('下架'),];
    }

    public function getFlagTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['flag']) ? $data['flag'] : '');
        $list = $this->flaglist();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->statuslist();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getUnitTextAttr($value, $data)
    {
        $unitid = isset($data['unitid']) ? $data['unitid'] : 0;
        return model('Product.Unit')->where(['id' => $unitid])->value('name');
    }

    public function getThumbTextAttr($value, $data)
    {
        //获取到cdn的地址
        $cdnurl = config('site.cdnurl') ? config('site.cdnurl') : '';
        $cdnurl = trim($cdnurl, '/');
        
        // 多张图字符串结构
        $thumbs = isset($data['thumbs']) ? $data['thumbs'] : '';

        if(empty($thumbs))
        {
            $thumbs = "/assets/img/shop.jpg";
        }else
        {
            $thumbs = explode(',', $thumbs);
            if(!empty($thumbs))
            {
                $pic = '';

                foreach($thumbs as $item)
                {
                    if(is_file(".".$item))
                    {
                        $pic = $item;
                        break;
                    }
                }

                if(empty($pic))
                {
                    $pic = "/assets/img/shop.jpg";
                }

                $thumbs = $pic;
            }
        }

        return $cdnurl.$thumbs;
    }

    public function getThumbsTextAttr($value, $data)
    {
        //获取到cdn的地址
        $cdnurl = config('site.cdnurl') ? config('site.cdnurl') : '';
        $cdnurl = trim($cdnurl, '/');
        
        // 多张图字符串结构
        $thumbs = isset($data['thumbs']) ? $data['thumbs'] : '';
        $arr = explode(',', $thumbs);

        //返回结果
        $list = [];

        foreach($arr as $item)
        {
            if(is_file(".".$item))
            {
                $list[] = $cdnurl.$item;
            }
        }

        if(empty($list))
        {
            $list[] = $cdnurl."/assets/img/shop.jpg";
        }

        return $list;
    }

    // 分类关联查询
    public function type()
    {
        return $this->belongsTo('app\common\model\Product\Type','typeid','id',[],'LEFT')->setEagerlyType(0);
    }

    // 单位关联查询
    public function unit()
    {
        return $this->belongsTo('app\common\model\Product\Unit','unitid','id',[],'LEFT')->setEagerlyType(0);
    }
}
