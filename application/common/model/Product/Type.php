<?php

namespace app\common\model\Product;

use think\Model;


class Type extends Model
{
    // 表名
    protected $name = 'product_type';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'thumb_text'
    ];

    public function getThumbTextAttr($value, $data)
    {
        //获取到cdn的地址
        $cdnurl = config('site.cdnurl') ? config('site.cdnurl') : '';
        $cdnurl = trim($cdnurl, '/');

        $thumb = isset($data['thumb']) ? $data['thumb'] : '';

        //如果为空就给一个默认图片地址
        if (empty($thumb) || !@is_file("." . $thumb)) {
            $thumb = "/assets/img/type.png";
        }

        return $cdnurl . $thumb;
    }
    // 关联查询
    public function product()
    {
        return $this->hasMany('app\common\model\Product\Product', 'typeid', 'id');
    }
}
