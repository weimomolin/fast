<?php

namespace app\common\model\Subject;

use think\Model;

class Chapter extends Model
{
    //设置表名
    protected $name = "subject_chapter";
    //指定一个自动设置的时间字段
    
    //开启自动写入
    protected $autoWriteTimestamp = true;

    //设置字段的名字
    protected $createTime = "createtime"; //插入的时候设置的字段名

    //禁止 写入的时间字段
    protected $updateTime = false;


    public function subject()
    {
        // chapter.subid = Subject.id
        // $this->belongsTo(关联外键模型,外键字段,关联表的主键,废弃参数,链表方式);
        // setEagerlyType(0)  采用join的方式来做查询
        return $this->belongsTo('app\common\model\Subject\Subject', 'subid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
