<?php

namespace app\common\model\Subject;

use think\Model;

class Comment extends Model
{
    protected $name = "subject_comment";

    //定义一个关联查询的方法//查询分类
    public function business()
    {
        // comment.busid = business.id
        // $this->belongsTo(关联外键模型,外键字段,关联表的主键,废弃参数,链表方式);
        // setEagerlyType(0)  采用join的方式来做查询
        return $this->belongsTo('app\common\model\Business\Business', 'busid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
