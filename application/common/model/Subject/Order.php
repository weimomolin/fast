<?php

namespace app\common\model\Subject;

use think\Model;
// 引入软删除的模型
use traits\model\SoftDelete;

class Order extends Model
{
    //设置表名
    protected $name = "subject_order";
    //在模型内部去引入软删除
    use SoftDelete;
    //设置软删除的字段更新的时间字段
    protected $deleteTime = 'deletetime';
    //指定一个自动设置的时间字段
    //开启自动写入
    protected $autoWriteTimestamp = true;

    //设置字段的名字
    protected $createTime = "createtime"; //插入的时候设置的字段名

    //禁止 写入的时间字段
    protected $updateTime = false;


    public function business()
    {
        // order.busid = business.id
        // $this->belongsTo(关联外键模型,外键字段,关联表的主键,废弃参数,链表方式);
        // setEagerlyType(0)  采用join的方式来做查询
        return $this->belongsTo('app\common\model\Business\Business', 'busid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function subject()
    {
        // order.busid = business.id
        // $this->belongsTo(关联外键模型,外键字段,关联表的主键,废弃参数,链表方式);
        // setEagerlyType(0)  采用join的方式来做查询
        return $this->belongsTo('app\common\model\Subject\Subject', 'subid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
