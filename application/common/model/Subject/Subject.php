<?php

namespace app\common\model\Subject;

use think\Model;
// 引入软删除的模型
use traits\model\SoftDelete;

class Subject extends Model
{
    // 需要连接的数据库表名
    protected $name = "subject";
    //在模型内部去引入软删除
    use SoftDelete;
    //设置软删除的字段更新的时间字段
    protected $deleteTime = 'deletetime';


    // 获取器(相当于数据库返回的数据进行追加操作)
    protected $append = [
        'thumbs_text',         // 课程封面地址（默认封面）
        'createtime_text',     // 课程创建时间（时间戳转换成年月日）
        'likes_text'           // 点赞数量（string转换array，统计长度）
    ];

    // 追加课程默认封面
    public function getThumbsTextAttr($value, $data)
    {
        // 判断原本thumbs字段有没有值
        $thumbs = isset($data['thumbs']) ? $data['thumbs'] : '';

        // 当thumbs中没有值，并且图片文件不存在时，将默认图片地址赋值
        if (empty($thumbs) || !is_file("." . $thumbs)) {
            $thumbs = '/assets/home/images/video.jpg';
        }
        return $thumbs;
    }

    // 转换课程时间（数据库中存放的是时间戳，需要转换成年-月-日格式）
    public function getCreateTimeTextAttr($value, $data)
    {
        $createtime = isset($data['createtime']) ? trim($data['createtime']) : '';

        if (empty($createtime)) {
            return '';
        }

        return date("Y-m-d H:i", $createtime);
    }


    // 数据库 通过存放一个id字符串 "id1,id2,id3,id4,id5" 标识当前课程点赞用户的id
    // 使用时，需要先将字符串数据转换成数组格式，统计数组长度可以得出点赞数
    public function getLikesTextAttr($value, $data)
    {
        $likes = isset($data['likes']) ? trim($data['likes']) : '';

        if (empty($likes)) {
            return 0;
        }

        //将字符串变成数组
        $arr = explode(',', $likes);

        //统计数组的长度，就是点赞人的个数
        return count($arr);
    }

    //定义一个关联查询的方法//查询分类
    public function category()
    {
        // subject.cateid = category.id
        // $this->belongsTo(关联外键模型,外键字段,关联表的主键,废弃参数,链表方式);
        // setEagerlyType(0)  采用join的方式来做查询
        return $this->belongsTo('app\common\model\Subject\Category', 'cateid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
