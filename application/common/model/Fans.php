<?php

namespace app\common\model;

use think\Model;

/**
 * 关注粉丝模型
 */
class Fans extends Model
{
    protected $name = 'fans';

    public function business()
    {
        return $this->belongsTo('app\common\model\Business\Business', 'busid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function fanslist()
    {
        return $this->belongsTo('app\common\model\Business\Business', 'fansid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
