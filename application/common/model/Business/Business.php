<?php

namespace app\common\model\Business;

use think\Model;
// 引入软删除的模型
use traits\model\SoftDelete;

/**
 * 客户模型
 */
class Business extends Model
{
    //标志当前模型操作的是哪张表
    protected $name = "business";
    //在模型内部去引入软删除
    use SoftDelete;
    //开启自动写入
    protected $autoWriteTimestamp = true;

    //设置字段的名字
    protected $createTime = "createtime"; //插入的时候设置的字段名
    protected $deleteTime = 'deletetime';

    //设置更新的字段名字
    protected $updateTime = false; //在更新数据的时候，不更新时间字段

    //增加的获取器字段选项
    protected $append = [
        'avatar_text',
        'province_text', // 省
        'city_text', // 市
        'district_text', // 区
        'sex_text', //性别
        'gender_text',
        'region_text', //地区字符串,
        'deal_text', //成交状态
    ];

    // 省
    public function getProvinceTextAttr($value, $data)
    {
        $region = empty($data['province']) ? '' : trim($data['province']);

        //查询中文字
        return model('common/Region')->where(['code' => $region])->value('name');
    }

    // 市
    public function getCityTextAttr($value, $data)
    {
        $region = empty($data['city']) ? '' : trim($data['city']);

        //查询中文字
        return model('common/Region')->where(['code' => $region])->value('name');
    }

    // 区
    public function getDistrictTextAttr($value, $data)
    {
        $region = empty($data['district']) ? '' : trim($data['district']);

        //查询中文字
        return model('common/Region')->where(['code' => $region])->value('name');
    }

    //增加对应的获取器的方法 命名有规律 输出的时候才会叫做avatar_text
    // $value 当前字段的值
    // $data 是一整条数据结构
    public function getAvatarTextAttr($value, $data)
    {
        //获取到cdn的地址
        $cdnurl = config('site.cdnurl') ? config('site.cdnurl') : '';
        $cdnurl = trim($cdnurl, '/');
        $avatar = isset($data['avatar']) ? $data['avatar'] : '';

        //如果为空就给一个默认图片地址
        if (empty($avatar) || !is_file("." . $avatar)) {
            $avatar = "/assets/img/avatar.png";
        }
        return $cdnurl . $avatar;
    }
    public function getSexTextAttr($value, $data)
    {
        $sexlist = [0 => '保密', 1 => '男', 2 => '女'];

        $gender = isset($data['gender']) ? $data['gender'] : '';

        if ($gender >= '0') {
            return $sexlist[$gender];
        }
        return;
    }
    public function getDealTextAttr($value, $data)
    {
        $sexlist = [0 => '未成交', 1 => '已成交'];

        $deal = isset($data['deal']) ? $data['deal'] : '';

        if ($deal >= '0') {
            return $sexlist[$deal];
        }
        return;
    }
    public function getMobileTextAttr($value, $data)
    {
        $mobile = isset($data['mobile']) ? $data['mobile'] : '';

        return substr_replace($mobile, '****', 3, 4);
    }

    // 将性别转换成中文
    public function getGenderTextAttr($value, $data)
    {
        $gender = $data['gender'] ? $data['gender'] : 0;

        $list = ['0' => '保密', '1' => '男', '2' => '女'];

        return $list[$gender];
    }

    public function getRegionTextAttr($value, $data)
    {
        $region = '';

        // 省
        $province = empty($data['province']) ? '' : $data['province'];
        if ($province) {
            $province_text = model('common/Region')::where('code', $province)->value('name');
            $region = $province_text . "-";
        }

        // 市
        $city = empty($data['city']) ? '' : $data['city'];
        if ($city) {
            $city_text = model('common/Region')::where('code', $city)->value('name');
            $region .= $city_text . "-";
        }

        // 区
        $district = empty($data['district']) ? '' : $data['district'];
        if ($district) {
            $district_text = model('common/Region')::where('code', $district)->value('name');
            $region .= $district_text;
        }

        //广东省-广州市-海珠区
        return $region;
    }

    //定义一个关联查询的方法//查询分类
    public function source()
    {
        // business.sourceid = source.id
        // $this->belongsTo(关联外键模型,外键字段,关联表的主键,废弃参数,链表方式);
        // setEagerlyType(0)  采用join的方式来做查询
        return $this->belongsTo('app\common\model\Business\Source', 'sourceid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'adminid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function provinces()
    {
        // belongsTo('关联模型名','外键名','关联表主键名',['模型别名定义'],'join类型');
        //参数1：关联的模型
        //参数2：用户表的外键的字段
        //参数3：关联表的主键
        //参数4：模型别名
        //参数5：链接方式 left
        // setEagerlyType(1) IN查询
        // setEagerlyType(0) JOIN查询
        return $this->belongsTo('app\common\model\Region', 'province', 'code', [], 'LEFT')->setEagerlyType(0);
    }

    //查询城市
    public function citys()
    {
        return $this->belongsTo('app\common\model\Region', 'city', 'code', [], 'LEFT')->setEagerlyType(0);
    }

    //查询地区
    public function districts()
    {
        return $this->belongsTo('app\common\model\Region', 'district', 'code', [], 'LEFT')->setEagerlyType(0);
    }
}
