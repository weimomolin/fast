<?php

namespace app\common\model\Business;

use think\Model;

class Record extends Model
{
       //模型对应的是哪张表
       protected $name = "business_record";

       //指定一个自动设置的时间字段
       //开启自动写入
       protected $autoWriteTimestamp = true;

       //设置字段的名字
       protected $createTime = "createtime"; //插入的时候设置的字段名

       //禁止 写入的时间字段
       protected $updateTime = false;

       // 获取器 修改创建时间为年月日
       protected $append = [
              'createtime_text',     // 创建时间（时间戳转换成年月日）
       ];
       // 转换时间（数据库中存放的是时间戳，需要转换成年-月-日格式）
       public function getCreateTimeTextAttr($value, $data)
       {
              $createtime = isset($data['createtime']) ? trim($data['createtime']) : '';

              if (empty($createtime)) {
                     return '';
              }

              return date("Y-m-d H:i", $createtime);
       }
}
