<?php

namespace app\common\model\Business;

use think\Model;

/**
 * 客户来源模型
 */
class Source extends Model
{
    //设置该模型对应哪个数据库表
    protected $name = "business_source";
}
