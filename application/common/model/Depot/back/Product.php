<?php

namespace app\common\model\Depot\back;

use think\Model;

class Product extends Model
{
    // 表名 => 退货商品关联表
    protected $name = 'depot_back_product';

    /**
     * 关联查询 商品
    */
    public function products()
    {
        return $this->belongsTo('app\common\model\Product\Product','proid','id',[],'LEFT')->setEagerlyType(0);
    }
}
