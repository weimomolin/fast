<?php

namespace app\common\model\Depot\storage;

use think\Model;

class Product extends Model
{
    // 表名 => 入库商品关联表
    protected $name = 'depot_storage_product';

    /**
     * 关联查询 商品
    */
    public function products()
    {
        return $this->belongsTo('app\common\model\Product\Product','proid','id',[],'LEFT')->setEagerlyType(0);
    }
}
