<?php

namespace app\common\controller;

use think\Controller;

/**
 * 前台公共控制器
 * 主要实现功能
 *    登录信息验证（判断用户是否已经登录，防止非法访问）
 */
class Home extends Controller
{
    /**
     * 将验证方法封装起来，通过数组来控制是否执行验证方法
     * 公开界面（不需要用户登录就能访问的界面）不用执行验证方法，将其控制器中的方法名记录到公共数组中
     * 通过判断数组中的方法名，实现控制特定界面不执行验证方法的效果
     */

    // 存放不执行验证登录的界面
    public $NoCheckLogin = [];

    // 构造函数
    public function __construct()
    {
        // 获取父类构造方法中的所有东西
        parent::__construct();

        // 加载用户模型（用于后续判断）
        $this->BusinessModel = model('Business.Business');

        // 返回当前的模块，控制器，方法
        // var_dump($this->request->module());
        // var_dump($this->request->controller());
        // var_dump($this->request->action());
        // echo $this->request->action();

        // 获取当前访问的控制器中的方法名（人话：获取当前界面 一个方法名对应一个界面）
        $action = $this->request->action();

        /**
         * 判断当前界面需不需要显示（Ps: 不显示的界面，放在$NoCheckLogin[]里面）
         * 不在$NoCheckLogin[]里面的界面全部需要验证登录状态
         * $NoCheckLogin[] 中有两种情况：    1、存的方法名     2、* 号，代表当前控制器内的所有方法（界面）名
         */
        if (!in_array($action, $this->NoCheckLogin) && !in_array('*', $this->NoCheckLogin)) {
            // 调用下面封装好的验证函数
            $this->CheckLogin();
        }
    }

    /**
     * 验证是否登录
     * @param $redirect true 会跳转   false 不会跳转  
     * 利用变量来控制验证完成后，是否需要跳转界面（跳转到登录界面）
     * 默认为 true  验证完成后跳转界面
     * @return 返回用户信息
     */
    public function CheckLogin($redirect = true)
    {
        // 获取cookie中存放的用户数据 id mobile
        $cookie = cookie('business');

        // 取出id和mobile
        // 先判断是否存在 然后存到变量中去
        $id = isset($cookie['id']) ? trim($cookie['id']) : 0;
        $mobile = isset($cookie['mobile']) ? trim($cookie['mobile']) : '';

        // 在数据库中匹配信息
        // where条件
        $where = ['id' => $id, 'mobile' => $mobile];
        // 执行条件查询
        $business = $this->BusinessModel->where($where)->find();

        /**
         * 通过返回值进行判断
         * 找到数据  返回 影响行数 
         * 没有找到  返回 0
         */
        if (!$business) {
            // 返回值为假，说明没有找到用户，说明cookie是假的，直接删除
            cookie('business', null);

            // 判断是否需要跳转界面
            if ($redirect) {
                $this->error('非法访问，请重新登录', url('home/index/login'));
                exit;
            } else {
                return null;
            }
        }

        // 如果要是找到了用户信息，那么我们要渲染数据
        if ($redirect) {
            $this->view->assign('business', $business);
        }

        return $business;
    }
}
