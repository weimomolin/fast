<?php

namespace app\common\validate\Post;


//引入Thinkphp底层验证器进来
use think\Validate;

/**
 * 定义客户表的验证器
 */
class Post extends Validate
{
    /**
     * 设置我们要验证字段的规则
     */
    protected $rule = [
        'title' => ['require'],
        'content' => ['require'],
        'point' => ['number', '>=:0'],
        'status' => ['in:0,2'],
        'busid' => ['require'],
        'cateid' => ['require'],
    ];

    /**
     * 设置错误的提醒信息
     */
    protected $message = [
        'title.require' => '帖子标题必填',
        'content.require'  => '帖子内容必填',
        'point.number'      => '悬赏积分必须是数字类型',
        'point.>='      => '悬赏积分必须大于等于0',
        'status.in'      => '解决状态的值有误',
        'busid.require'  => '发帖人id必填',
        'cateid.require'  => '帖子分类id必填',
    ];

    /**
     * 设置验证器的场景
     */
    protected $scene = [
       
    ];
}
