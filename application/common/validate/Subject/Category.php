<?php
namespace app\common\validate\Subject;

use think\Validate;

// 课程分类的验证器
class Category extends Validate
{
    protected $rule =   [
        'weight' => ['require', 'number','egt:0'], 
    ];

    protected $message  =   [  
        'weight.require'  => '课程分类权重必填', 
        'weight.number'  => '课程分类权重必须数字',   
        'weight.egt'  => '课程分类权重必须大于等于0',   
    ];
}