<?php

namespace app\ask\controller;

use think\Controller;
use think\Request;
// 引入FastAdmin自带的一个s邮箱发送类
use app\common\library\Email;

class Business extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->BusinessModel = model('Business.Business');
        $this->PostModel = model('Post.Post');
        $this->CommentModel = model('Post.Comment');
        $this->CollectModel = model('Post.Collect');
        $this->FansModel = model('Fans');
    }


    //---------------------------------------------- 微信小程序api ----------------------------------------------

    /**
     * 用户授权登录方法
     * 获取到临时凭证code，通过code去向微信公众平台换取unionid（需要微信公众平台认证，营业执照）
     * unionid拿不到，退而求其次获取openId字段，判断数据库中释放后存在openId
     * 存在，查询出用户信息并返回出去
     * 不存在，跳转到login界面
     */
    public function login()
    {
        if ($this->request->isPost()) {
            // 获取临时凭证
            
            $code = $this->request->param('code', '', 'trim');

            if (empty($code)) {
                $this->error('临时凭证获取失败');
                exit;
            }

            //发送请求给微信公众平台 获取openid
            $wxauth = $this->code2Session($code);
            $openid = isset($wxauth['openid']) ? trim($wxauth['openid']) : '';

            if (empty($openid)) {
                $this->error('授权失败');
                exit;
            }

            // 通过openid查询数据库中用户信息
            $business = $this->BusinessModel->where(['openid' => $openid])->find();
            unset($business['password']);
            unset($business['salt']);

            if ($business) {
                // 用户信息存在，说明数据库中openid字段有数据，无需后续其他操作
                $this->success('授权登录成功', null, $business);
                exit;
            }
            // 用户信息不存在，说明需要用户去进行注册
            $this->success('授权成功，请绑定账号', "/pages/business/login", ['action' => 'bind', 'openid' => $openid]);
            exit;
        }
    }

    /**
     * 用户绑定账号方法（用户注册）
     * 前端表单输入手机号密码，查询数据库中是否有改手机号
     * 有，再判断是否存在openId数据，有，提示该用户已绑定，没有，将openId插入到数据库，返回一个状态
     * 没有，将用户数据插入到数据库中（注意加上openId数据）
     */
    public function bind()
    {
        if ($this->request->isPost()) {
            $openid = $this->request->param('openid', '', 'trim');
            $password = $this->request->param('password', '', 'trim');
            $mobile = $this->request->param('mobile', '', 'trim');

            $business = $this->BusinessModel->where(['mobile' => $mobile])->find();

            //如果找得到就说明绑定过， 如果找不到就说明账号不存在，就注册插入
            if ($business) {
                // 数据库中用户的openid本来就存在时
                if (!empty($business['openid'])) {
                    $this->error('该用户已绑定，无法重复绑定');
                    exit;
                }

                // 有用户信息，但openid字段没有数据，说明没有授权，在其他平台注册的账号，将openid更新到数据库中
                $data = [
                    'id' => $business['id'],
                    'openid' => $openid
                ];

                // 更新语句
                $result = $this->BusinessModel->isUpdate(true)->save($data);

                if ($result === FALSE) {
                    $this->error('绑定账号失败');
                    exit;
                } else {
                    $business['openid'] = $openid;
                    $this->success('绑定账号成功', null, $business);
                    exit;
                }
            } else {
                // 用户数据不存在，说明当前用户是第一次访问，在其他平台中没有注册账号
                // 将用户信息插入到数据库中
                if (empty($password)) {
                    $this->error('密码不能为空');
                    exit;
                }

                //生成一个密码盐
                $salt = randstr();

                //加密密码
                $password = md5($password . $salt);

                //组装数据
                $data = [
                    'openid' => $openid,
                    'mobile' => $mobile,
                    'nickname' => $mobile,
                    'password' => $password,
                    'salt' => $salt,
                    'gender' => '0', //性别
                    'deal' => '0', //成交状态
                    'money' => '0', //余额
                    'auth' => '0', //实名认证
                ];

                // 查询出当前用户来源数据
                $data['sourceid'] = model('common/Business/Source')->where(['name' => ['LIKE', "%知识社区小程序%"]])->value('id');

                // 执行插入 返回自增的条数
                $result = $this->BusinessModel->validate('common/Business/Business')->save($data);

                if ($result === FALSE) {
                    // 失败
                    $this->error($this->BusinessModel->getError());
                    exit;
                } else {
                    // 查询出当前插入的数据记录 
                    // $this->BusinessModel->id 代表当前表新增的记录id值
                    $business = $this->BusinessModel->find($this->BusinessModel->id);

                    // 返回结果
                    $this->success('注册成功', null, $business);
                    exit;
                }
            }
        }
    }

    /**
     * 解绑账号
     * 拿到前端传递过来的用户信息，通过用户id将数据库中对应用户的openid设置为null
     */
    public function unbind()
    {
        if ($this->request->isPost()) {
            $busid = $this->request->param('busid', '', 'trim');
            $openid = $this->request->param('openid', '', 'trim');

            // 判断用户是否存在
            $business = $this->BusinessModel->find($busid);

            // 用户不存在
            if (!$business) {
                $this->error('用户不存在');
                exit;
            }

            // 判断当前用户信息openid是否存在
            if ($openid == '') {
                $this->error('解绑失败，当前用户未授权');
                exit;
            }

            // 解绑 将openid设置为null 更新操作
            $data = [
                'id' => $busid,
                'openid' => null
            ];
            $result = $this->BusinessModel->isUpdate(true)->save($data);

            if ($result === FALSE) {
                $this->error('解绑账号失败');
                exit;
            } else {
                $business = $this->BusinessModel->find($busid);
                $this->success('解绑账号成功', null, $business);
                exit;
            }
        }
    }

    /**
     * 用户授权登录方法
     * 获取到临时凭证code，通过code去向微信公众平台换取unionid（需要微信公众平台认证，营业执照）
     * unionid拿不到，退而求其次获取openId字段，判断数据库中释放后存在openId
     * 存在，查询出用户信息并返回出去
     * 不存在，跳转到login界面
     */
    public function munbind()
    {
        if ($this->request->isPost()) {
            // 获取临时凭证
            $code = $this->request->param('code', '', 'trim');
            $busid = $this->request->param('busid', '', 'trim');

            // 判断用户是否存在
            $business = $this->BusinessModel->find($busid);

            // 用户不存在
            if (!$business) {
                $this->error('用户不存在');
                exit;
            }

            // 判断临时凭证是否存在
            if (empty($code)) {
                $this->error('临时凭证获取失败');
                exit;
            }

            //发送请求给微信公众平台 获取openid
            $wxauth = $this->code2Session($code);

            $openid = isset($wxauth['openid']) ? trim($wxauth['openid']) : '';

            if (empty($openid)) {
                $this->error('授权失败');
                exit;
            }

            // 通过openid查询数据库中用户信息
            $business = $this->BusinessModel->where(['openid' => $openid])->find();

            if ($business) {
                // 用户信息存在，说明该用户已经授权过别的账号了
                $this->error('绑定账号失败，该微信号已绑定其他账号');
                exit;
            }

            // 用户信息不存在，将对应用户的openid更新
            $data = [
                'id' => $busid,
                'openid' => $openid
            ];

            $result = $this->BusinessModel->isUpdate(true)->save($data);

            if ($result === FALSE) {
                $this->error('绑定账号失败');
                exit;
            } else {
                $business = $this->BusinessModel->find($busid);
                $this->success('绑定账号成功', null, $business);
                exit;
            }
        }
    }

    /**
     * 用户编辑资料方法
     * 必须是用户有修改头像，上传头像文件时执行
     */
    public function profile()
    {
        //判断是否有Post过来数据
        if ($this->request->isPost()) {
            //可以一次性接收到全部数据
            $id = $this->request->param('id', 0, 'trim');
            $nickname = $this->request->param('nickname', '', 'trim');
            $mobile = $this->request->param('mobile', '', 'trim');
            $email = $this->request->param('email', '', 'trim');
            $gender = $this->request->param('gender', '0', 'trim');
            $code = $this->request->param('code', '', 'trim');
            $password = $this->request->param('password', '', 'trim');

            //判断用户是否存在
            $business = $this->BusinessModel->find($id);

            if (!$business) {
                $this->error('用户不存在');
                exit;
            }

            // 直接组装数据
            $data = [
                'id' => $business['id'], //因为我们要执行更新语句
                'nickname' => $nickname,
                'mobile' => $mobile,
                'gender' => $gender,
            ];

            //如果密码不为空 修改密码
            if (!empty($password)) {
                //重新生成一份密码盐
                $salt = randstr();

                $data['salt'] = $salt;
                $data['password'] = md5($password . $salt);
            }

            //判断是否修改了邮箱 输入的邮箱 不等于 数据库存入的邮箱
            //如果邮箱改变，需要重新认证
            if ($email != $business['email']) {
                $data['email'] = $email;
                $data['auth'] = '0';
            }

            //判断是否有地区数据
            if (!empty($code)) {
                //查询省市区的地区码出来
                $parent = model('Region')->where(['code' => $code])->value('parentpath');

                if (!empty($parent)) {
                    $arr = explode(',', $parent);
                    $data['province'] = isset($arr[0]) ? $arr[0] : null;
                    $data['city'] = isset($arr[1]) ? $arr[1] : null;
                    $data['district'] = isset($arr[2]) ? $arr[2] : null;
                }
            }

            //判断是否有图片上传
            if (isset($_FILES['avatar']) && $_FILES['avatar']['error'] == 0) {
                $success = build_upload('avatar');

                //如果上传失败，就提醒
                if (!$success['result']) {
                    $this->error($success['msg']);
                    exit;
                }

                //如果上传成功
                $data['avatar'] = $success['data'];
            }

            //执行更新语句 数据验证 -> 需要用到验证器

            //这是插入语句
            // $result = $this->BusinessModel->validate('common/Business/Business')->save($data);

            //更新语句 如果是更新语句，需要给data提供一个主键id的值 这就是更新语句 使用验证器的场景
            $result = $this->BusinessModel->validate('common/Business/Business.profile')->isUpdate(true)->save($data);

            if ($result === FALSE) {
                $this->error($this->BusinessModel->getError());
                exit;
            }

            //判断是否有旧图片，如果有就删除
            if (isset($data['avatar'])) {
                is_file("." . $business['avatar']) && @unlink("." . $business['avatar']);
            }

            $business = $this->BusinessModel->find($id);

            unset($business['password']);
            unset($business['salt']);

            $this->success('更新资料成功', null, $business);
            exit;
        }
    }

    /**
     * 用户编辑资料方法
     * 用户没有修改头像，只上传非文件数据时执行
     */
    public function profileNotAva()
    {
        //判断是否有Post过来数据
        if ($this->request->isPost()) {
            //可以一次性接收到全部数据
            $id = $this->request->param('id', 0, 'trim');
            $nickname = $this->request->param('nickname', '', 'trim');
            $mobile = $this->request->param('mobile', '', 'trim');
            $email = $this->request->param('email', '', 'trim');
            $gender = $this->request->param('gender', '0', 'trim');
            $code = $this->request->param('code', '', 'trim');
            $password = $this->request->param('password', '', 'trim');

            //判断用户是否存在
            $business = $this->BusinessModel->find($id);

            if (!$business) {
                $this->error('用户不存在');
                exit;
            }

            // 直接组装数据
            $data = [
                'id' => $business['id'], //因为我们要执行更新语句
                'nickname' => $nickname,
                'mobile' => $mobile,
                'gender' => $gender,
            ];

            //如果密码不为空 修改密码
            if (!empty($password)) {
                //重新生成一份密码盐
                $salt = randstr();

                $data['salt'] = $salt;
                $data['password'] = md5($password . $salt);
            }

            //判断是否修改了邮箱 输入的邮箱 不等于 数据库存入的邮箱
            //如果邮箱改变，需要重新认证
            if ($email != $business['email']) {
                $data['email'] = $email;
                $data['auth'] = '0';
            }

            //判断是否有地区数据
            if (!empty($code)) {
                //查询省市区的地区码出来
                $parent = model('Region')->where(['code' => $code])->value('parentpath');

                if (!empty($parent)) {
                    $arr = explode(',', $parent);
                    $data['province'] = isset($arr[0]) ? $arr[0] : null;
                    $data['city'] = isset($arr[1]) ? $arr[1] : null;
                    $data['district'] = isset($arr[2]) ? $arr[2] : null;
                }
            }

            //执行更新语句 数据验证 -> 需要用到验证器

            //这是插入语句
            // $result = $this->BusinessModel->validate('common/Business/Business')->save($data);

            //更新语句 如果是更新语句，需要给data提供一个主键id的值 这就是更新语句 使用验证器的场景
            $result = $this->BusinessModel->validate('common/Business/Business.profile')->isUpdate(true)->save($data);

            if ($result === FALSE) {
                $this->error($this->BusinessModel->getError());
                exit;
            }

            $business = $this->BusinessModel->find($id);

            unset($business['password']);
            unset($business['salt']);

            $this->success('更新资料成功', null, $business);
            exit;
        }
    }

    // ---------------- 会员资料邮箱验证码 ----------------
    public function email()
    {
        //加载模型
        $EmsModel = model('common/Ems');

        if ($this->request->isPost()) {

            $action = $this->request->param('action', '', 'trim');
            $email = $this->request->param('email', '', 'trim');

            if ($action == "send") {
                if (empty($email)) {
                    $this->error('邮箱地址为空');
                    exit;
                }

                //生成一个验证码
                $code = randstr(5);

                //开启事务
                $EmsModel->startTrans();

                //删除掉之前旧的验证码
                $EmsModel->where(['email' => $email])->delete();

                //把验证码插入到数据库表中
                $data = [
                    'event' => 'auth',
                    'email' => $email,
                    'code' => $code,
                    'times' => 0,
                ];

                //插入数据
                $ems = $EmsModel->save($data);

                if ($ems === FALSE) {
                    $this->error('邮件插入失败');
                    exit;
                }

                //邮件主题
                $name = config('site.name');
                $subject = "【{$name}】邮箱验证";

                //组装文字信息
                $message = "<div>感谢您的使用，您的邮箱验证码为：<b>$code</b></div>";

                //实例化邮箱验证类
                $PhpMailer = new Email;

                //邮箱发送有规律，不可以发送关键词
                $result = $PhpMailer
                    ->to($email)
                    ->subject($subject)
                    ->message($message)
                    ->send();

                //检测邮箱发送成功还是失败
                if ($result) {
                    //发送验证码成功
                    //将事务提交，提交的意思就是让刚刚插入的记录真实存在到数据表中
                    $EmsModel->commit();
                    $this->success('邮件发送成功，请注意查收');
                    exit;
                } else {
                    //将刚才插入成功的验证码记录要撤销回滚
                    $EmsModel->rollback();
                    $this->error($PhpMailer->getError());
                    exit;
                }
            } else {
                $code = $this->request->param('code', '', 'trim');
                $busid = $this->request->param('busid', '', 'trim');

                if (empty($code)) {
                    $this->error('验证码不能为空');
                    exit;
                }


                if (empty($email)) {
                    $this->error('邮箱不能为空');
                    exit;
                }

                //开启事务
                $EmsModel->startTrans();
                $this->BusinessModel->startTrans();

                //查询这个验证码是否存在
                $where = ['email' => $email, 'code' => $code];
                $check = $EmsModel->where($where)->find();

                //如果没找到记录
                if (!$check) {
                    $this->error('您输入的验证码有误，请重新输入');
                    exit;
                }

                // 1、更新用户表  2、删除验证码记录

                //组装数据
                $data = [
                    'id' => $busid,
                    'auth' => '1'
                ];

                //执行
                $BusinessStatus = $this->BusinessModel->isUpdate(true)->save($data);

                if ($BusinessStatus === FALSE) {
                    $this->error('用户邮箱认证状态修改失败');
                    exit;
                }

                //第二条 删除验证码记录
                $EmsStatus = $EmsModel->where($where)->delete();

                if ($EmsStatus === FALSE) {
                    //先要将用户表的更新进行回滚
                    $this->BusinessModel->rollback();
                    $this->error('验证码记录删除失败');
                    exit;
                }

                if ($BusinessStatus === FALSE || $EmsStatus === FALSE) {
                    $EmsModel->rollback();
                    $this->BusinessModel->rollback();
                    $this->error('验证失败');
                    exit;
                } else {
                    //提交事务
                    $this->BusinessModel->commit();
                    $EmsModel->commit();

                    $business = $this->BusinessModel->find($busid);
                    unset($business['password']);
                    unset($business['salt']);

                    $this->success('邮箱验证成功', null, $business);
                    exit;
                }
            }
        }
    }

    /**
     * 获取个人主页用户信息
     * 统计 发帖数量（提问）、评论数量（回答）、收藏数量（收藏）
     */
    public function getCenterData()
    {
        if ($this->request->isPost()) {
            $accessToBusid = $this->request->param('accessToBusid',0, 'trim');
            $busid = $this->request->param('busid', 0, 'trim');

            //判断所查询的用户是否存在
            $accessToBus = $this->BusinessModel->find($accessToBusid);

            if (!$accessToBus) {
                $this->error('所查询用户不存在');
                exit;
            }

            //判断登录用户是否存在
            $business = $this->BusinessModel->find($busid);

            if (!$business) {
                $this->error('登录用户不存在');
                exit;
            }

            // 获取发帖数量
            $PostCount = $this->PostModel->where(['busid' => $accessToBus['id']])->count();
            // 获取评论数量
            $CommentCount = $this->CommentModel->where(['busid' => $accessToBus['id']])->count();
            // 获取收藏数量
            $CollectCount = $this->CollectModel->where(['busid' => $accessToBus['id']])->count();
            // 查询关注状态
            $FansStatus = $this->FansModel->where(['busid'=>$accessToBus['id'],'fansid'=>$business['id']])->find();
         
            if(!empty($FansStatus)){
                $attStatus = true;
            }else{
                $attStatus = false;
            }
            // 组装数据
            $data = [
                'business' => $accessToBus,
                'PostCount' => $PostCount,
                'CommentCount' => $CommentCount,
                'CollectCount' => $CollectCount,
                'attStatus'=>$attStatus
            ];
            $this->success('查询成功', null, $data);
            exit;
        }
    }

    /**
     * tabs切换加载不同数据
     * 通过typename 区分不同情况，查询不同数据表
     * 我的回答（评论表）  我的提问（帖子表）   我的收藏（收藏表）
     * 返回的数据 
     */
    public function GetTabsCont()
    {
        if ($this->request->isPost()) {
            $busid = $this->request->param("busid", 0, 'trim');
            $typeName = $this->request->param("typename", '', 'trim');
            $page = $this->request->param('page', 1, 'trim');
            $limit = 10;

            $offset = ($page - 1) * $limit;

            //判断用户是否存在
            $business = $this->BusinessModel->find($busid);

            if (!$business) {
                $this->error('用户不存在');
                exit;
            }

            // 根据不同情况查询不同表
            // 查询所有我的发帖信息
            if ($typeName == '我的提问') {
                $list = $this->PostModel
                    ->with(['cate', 'business'])
                    ->where(['busid' => $busid])
                    ->order('createtime', 'desc')
                    ->limit($offset, $limit)
                    ->select();
            }

            // 先查询出评论过的帖子id
            if ($typeName == '我的回答') {
                $list = $this->PostModel
                    ->with(['cate', 'business', 'comment'])
                    ->where(['comment.busid' => $busid])
                    ->order('createtime', 'desc')
                    ->limit($offset, $limit)
                    ->group('id')
                    ->select();
            }

            if ($typeName == '我的收藏') {
                $list = $this->PostModel
                    ->with(['cate', 'business', 'collect'])
                    ->where(['collect.busid' => $busid])
                    ->order('createtime', 'desc')
                    ->limit($offset, $limit)
                    ->group('id')
                    ->select();
            }
            if ($list) {
                $this->success('返回帖子数据', null, $list);
                exit;
            } else {
                $this->error('暂无更多数据');
                exit;
            }
        }
    }

    //---------------------------------------------- web/app api接口 ----------------------------------------------

    //web注册登录方法
    public function web()
    {
        if ($this->request->isPost()) {
            $password = $this->request->param('password', '', 'trim');
            $mobile = $this->request->param('mobile', '', 'trim');

            $business = $this->BusinessModel->where(['mobile' => $mobile])->find();

            //如果找得到就说明绑定过， 如果找不到就说明账号不存在，就注册插入
            if ($business) {
                //验证密码是否正确
                $salt = $business['salt'];
                $password = md5($password . $salt);

                if ($password != $business['password']) {
                    $this->error('密码错误');
                    exit;
                } else {
                    unset($business['salt']);
                    unset($business['password']);
                    $this->success('登录成功', null, $business);
                    exit;
                }
            } else {
                //数据插入
                if (empty($password)) {
                    $this->error('密码不能为空');
                    exit;
                }

                //生成一个密码盐
                $salt = randstr();

                //加密密码
                $password = md5($password . $salt);

                //组装数据
                $data = [
                    'mobile' => $mobile,
                    'nickname' => $mobile,
                    'password' => $password,
                    'salt' => $salt,
                    'gender' => '0', //性别
                    'deal' => '0', //成交状态
                    'money' => '0', //余额
                    'auth' => '0', //实名认证
                ];

                //查询出云课堂的渠道来源的ID信息 数据库查询
                $data['sourceid'] = model('common/Business/Source')->where(['name' => ['LIKE', "%问答社区%"]])->value('id');

                //执行插入 返回自增的条数
                $result = $this->BusinessModel->validate('common/Business/Business')->save($data);

                if ($result === FALSE) {
                    //失败
                    $this->error($this->BusinessModel->getError());
                    exit;
                } else {
                    //查询出当前插入的数据记录
                    $business = $this->BusinessModel->find($this->BusinessModel->id);

                    unset($business['salt']);
                    unset($business['password']);

                    //注册
                    $this->success('注册成功', null, $business);
                    exit;
                }
            }
        }
    }



    //---------------------------------------------- 封装的方法 ----------------------------------------------

    /**
     * 封装调用微信官方获取用户信息方法
     * 
     * 通过code去向微信公众平台换取openId （查询微信公众平台接口https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/user-login/code2Session.html）
     * 定义api接口需要的参数
     * 
     * 小程序ID
     * $appid = 'wxfb457fc3dbde0e36';
     * 
     * 小程序密钥
     * $secret = '7da6486ffe47fca7a7b0fa76c074d3dd';
     * 
     * 临时凭证code（通过官方login接口获取的）
     * $js_code = $code;
     * 
     * 授权类型
     * $js_code = 'authorization_code';
     * 
     * 发起请求 GET https://api.weixin.qq.com/sns/jscode2session
     */
    public function code2Session($code)
    {

        if ($code) {

            // 改成自己的小程序 appid
            $appid = "wx1a24866296da0982";

            // 改成自己的小程序 appSecret
            $appSecret = "beddfb90f0e6870d60e0bbc3aa014c71";

            // 微信官方提供的接口，获取唯一的opendid
            $url = "https://api.weixin.qq.com/sns/jscode2session?appid=$appid&secret=$appSecret&js_code=$code&grant_type=authorization_code";

            $result = $this->https_request($url);

            //获取结果 将json转化为数组
            $resultArr = json_decode($result, true);

            return $resultArr;
        }
        return false;
    }

    // http请求 利用php curl扩展去发送get 或者 post请求 服务器上面一定要开启 php curl扩展
    // https://www.php.net/manual/zh/book.curl.php
    protected function https_request($url, $data = null)
    {
        if (function_exists('curl_init')) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
            if (!empty($data)) {
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            // 发送会话，返回结果
            $output = curl_exec($curl);
            curl_close($curl);
            return $output;
        } else {
            return false;
        }
    }
}
