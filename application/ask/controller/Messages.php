<?php

namespace app\ask\controller;

use think\Controller;

// 私信控制器
class Messages extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->MessagesModel = model('Post.Messages');
        $this->BusinessModel = model('Business.Business');
    }

    /**
     * 获取用户收到的私信
     * busid = recid
     * 并且要返回发信人的信息
     */
    public function index()
    {
        if ($this->request->isPost()) {
            $recid = $this->request->param('recid', 0, 'trim');
            $page = $this->request->param('page', 1, 'trim');
            $keywords = $this->request->param('keywords', '', 'trim');
            
            $limit = 10;
            $offset = ($page - 1) * $limit;

            $where = [];

            if ($recid) {
                $where['recid'] = $recid;
            }

            if (!empty($keywords)) {
                $where['sender.nickname'] = ['like', "%$keywords%"];
            }

            // 查找匿名用户私信
            $anonymous = preg_match('/匿/i',$keywords);
            if ($anonymous) {
                $where['sender.nickname'] = '';
            }

            $list = $this->MessagesModel
                ->with(['sender'])
                ->where($where)
                ->order('id', 'desc')
                ->limit($offset, $limit)
                ->select();

            if (!$list) {
                $this->error('暂无收到私信');
                exit;
            }
            $this->success('查询成功成功', null, $list);
            exit;
        }
    }

    /**
     * 发送私信
     * 往私信表中添加数据
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $senid = $this->request->param('senid', 0, 'trim'); // 发件人id  当前登录的用户id 
            $recid = $this->request->param('recid', 0, 'trim'); // 收件人id  当前界面显示的用户id
            $content = $this->request->param('content', '', 'trim'); // 私信内容

            $senStatus = $this->BusinessModel->find($senid);
            if (!$senStatus) {
                $this->error('发信用户不存在');
                exit;
            }

            $recStatus = $this->BusinessModel->find($recid);
            if (!$recStatus) {
                $this->error('收信用户不存在');
                exit;
            }

            if ($content === '') {
                $this->error('不可以发送空白私信');
                exit;
            }

            // 组装数据
            $data = [
                'senid' => $senid,
                'recid' => $recid,
                'content' => $content
            ];

            $result = $this->MessagesModel->save($data);

            if ($result) {
                $this->success('发送成功');
                exit;
            }
            $this->error('发送失败');
            exit;
        }
    }

    /**
     * 修改私信查看状态
     * 通过id修改状态
     */
    public function changeStatus()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id', 0, 'trim'); // 发件人id  当前登录的用户id 
            $status = $this->request->param('status', '0', 'trim'); // 收件人id  当前界面显示的用户id

            $message = $this->MessagesModel->find($id);

            if (!$message) {
                $this->error('私信不存在');
                exit;
            }
            $data = [
                'id' => $id,
                'status' => $status
            ];
            // 更新数据表
            $result = $this->MessagesModel->isUpdate(true)->save($data);

            if (!$result) {
                $this->error('私信状态修改失败');
                exit;
            }

            $this->success('私信状态修改成功');
            exit;
        }
    }

    /**
     * 删除私信
     * id
     */
    public function del()
    {
        if ($this->request->isPost()) {
            $busid = $this->request->param('busid', 0, 'trim');
            $id = $this->request->param('id', 0, 'trim');


            $business = $this->BusinessModel->find($busid);

            if (!$business) {
                $this->error('用户不存在');
                exit;
            }

            // 先判断有没有
            $result = $this->MessagesModel->find($id);

            // 有数据就删除
            if ($result) {

                $res = $this->MessagesModel->where(['id' => $result['id']])->delete();

                if ($res) {
                    $this->success('删除私信成功');
                    exit;
                }
                $this->error('删除私信失败');
                exit;
            }
            $this->error('删除私信失败');
            exit;
        }
    }
}
