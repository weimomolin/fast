<?php

namespace app\ask\controller;

use think\Controller;

class Comment extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->BusinessModel = model('Business.Business');
        $this->PostModel = model('Post.Post');
        $this->CommentModel = model('Post.Comment');
    }

    public function index()
    {
        if ($this->request->isPost()) {

            $postid = $this->request->param('postid', 0, 'trim');
            $pid = $this->request->param('pid', 0, 'trim');
            $post = $this->PostModel->find($postid);

            if (!$post) {
                $this->error('暂无帖子信息');
                exit;
            }

            $top = $this->CommentModel
                ->with(['business'])
                ->where(['postid' => $postid, 'pid' => $pid])
                ->order(['pid asc', 'id asc'])
                ->select();


            if(empty($top))
            {
                $this->error('暂无评论');
                exit;
            }

            //循环顶级
            foreach($top as $key=>&$item)
            {
                $item['children'] = $this->CommentModel->sublist($item['id']);
            }

            $this->success('返回评论列表', null, $top);
            exit;
        }
    }

    public function add()
    {
        if($this->request->isPost())
        {
            $pid = $this->request->param('pid', 0, 'trim');
            $content = $this->request->param('content', '', 'trim');
            $postid = $this->request->param('postid', 0, 'trim');
            $busid = $this->request->param('busid', 0, 'trim');

            $post = $this->PostModel->find($postid);

            if(!$post)
            {
                $this->error('暂无帖子信息');
                exit;
            }

            $business = $this->BusinessModel->find($busid);

            if(!$business)
            {
                $this->error('用户不存在');
                exit;
            }

            //组装数据
            $data = [
                'pid' => $pid,
                'content' => $content,
                'busid' => $busid,
                'postid' => $postid,
                'status' => '0',
            ];

            $result = $this->CommentModel->save($data);

            if($result === FALSE)
            {
                $this->error('评论失败');
                exit;
            }else
            {
                $this->success('评论成功');
                exit;
            }
        }
    }

    public function del()
    {
        if($this->request->isPost())
        {
            $comid = $this->request->param('comid', 0, 'trim');
            $postid = $this->request->param('postid', 0, 'trim');
            $busid = $this->request->param('busid', 0, 'trim');

            $post = $this->PostModel->find($postid);

            if(!$post)
            {
                $this->error('暂无帖子信息');
                exit;
            }

            $business = $this->BusinessModel->find($busid);

            if(!$business)
            {
                $this->error('用户不存在');
                exit;
            }

            $comment = $this->CommentModel->find($comid);

            if(!$comment)
            {
                $this->error('评论不存在');
                exit;
            }

            $tree = $this->CommentModel->subtree($comid);

            if(empty($tree))
            {
                $this->error('暂无删除的评论');
                exit;
            }

            $result = $this->CommentModel->where(['id' => ['IN', $tree]])->delete();

            if($result === FALSE)
            {
                $this->error('删除评论失败');
                exit;
            }else
            {
                $this->success('删除评论成功');
                exit;
            }
        }
    }
}
