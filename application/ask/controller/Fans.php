<?php

namespace app\ask\controller;

use think\Controller;

class Fans extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->model = model('Fans');
        $this->BusinessModel = model('Business.Business');
    }

    // 判断是否关注
    public function index()
    {
        if ($this->request->isPost()) {
            $busid = $this->request->param('busid', 0, 'trim');
            $fansid = $this->request->param('fansid', 0, 'trim');

            $business = $this->BusinessModel->find($busid);

            if (!$business) {
                $this->error('用户不存在');
                exit;
            }

            $result = $this->BusinessModel->find($fansid);

            if (!$result) {
                $this->error('用户不存在');
                exit;
            }

            $where = [
                'busid' => $fansid,
                'fansid' => $busid
            ];

            // 先判断有没有
            $result = $this->model->where($where)->find();

            if ($result) {
                $this->error('已关注', null, ['fansStatus' => true]);
                exit;
            }
            $this->success('关注成功', null, ['fansStatus' => false]);
            exit;
        }
    }
    public function add()
    {
        if ($this->request->isPost()) {
            $busid = $this->request->param('busid', 0, 'trim');
            $fansid = $this->request->param('fansid', 0, 'trim');

            $business = $this->BusinessModel->find($busid);

            if (!$business) {
                $this->error('用户不存在');
                exit;
            }

            $result = $this->BusinessModel->find($fansid);

            if (!$result) {
                $this->error('用户不存在');
                exit;
            }

            // 组装数据
            // 我关注别人  我是别人的粉丝  fansid = $ busid 
            $data = [
                'busid' => $fansid,
                'fansid' => $busid
            ];

            $where = [
                'busid' => $fansid,
                'fansid' => $busid
            ];

            // 先判断有没有
            $result = $this->model->where($where)->find();

            if ($result) {
                $this->error('已关注，请勿重复点击', null, ['fansStatus' => true]);
                exit;
            }

            $result = $this->model->save($data);

            if ($result) {
                $this->success('关注成功', null, ['fansStatus' => true]);
                exit;
            }
            $this->error('关注失败', null, ['fansStatus' => false]);
            exit;
        }
    }
    // 取消关注
    public function del()
    {
        if ($this->request->isPost()) {
            $busid = $this->request->param('busid', 0, 'trim');
            $fansid = $this->request->param('fansid', 0, 'trim');

            $business = $this->BusinessModel->find($busid);

            if (!$business) {
                $this->error('用户不存在');
                exit;
            }

            $result = $this->BusinessModel->find($fansid);

            if (!$result) {
                $this->error('用户不存在');
                exit;
            }

            $where = [
                'busid' => $fansid,
                'fansid' => $busid
            ];

            // 先判断有没有
            $result = $this->model->where($where)->find();

            // 有数据就删除
            if ($result) {

                $res = $this->model->where(['id' => $result['id']])->delete();

                if ($res) {
                    $this->success('取消关注', null, ['fansStatus' => false]);
                    exit;
                } else {
                    $this->error('取消关注失败');
                    exit;
                }
            }
            $this->error('取消关注失败');
            exit;
        }
    }

    /**
     * 我的关注
     * 两种情况 
     * 我的关注 通过fansid 查询所有的busid
     * 我的粉丝 通过busid  查询所有的fansid
     */
    public function follow()
    {
        if ($this->request->isPost()) {
            $page = $this->request->param('page', 1, 'trim');
            $typeid = $this->request->param('typeid', 0, 'trim');
            $keywords = $this->request->param('keywords', '', 'trim');
            $busid = $this->request->param('busid', 0, 'trim');

            $limit = 10;
            $offset = ($page - 1) * $limit;

            $where = [];

            if (!empty($keywords)) {
                $where['nickname'] = ['like', "%$keywords%"];
            }
            if ($keywords === '匿名') {
                $where['nickname'] = '';
            }

            // 分类id为0 我的关注
            if ($typeid == 0) {
                // 我的关注 意味着我是别人的粉丝，fansid = $busid ——> 找 busid
                if ($busid != 0) {
                    $where['fansid'] = $busid;
                }

                $list = $this->model
                    ->with(['business'])
                    ->where($where)
                    ->order('id', 'desc')
                    ->limit($offset, $limit)
                    ->group('id')
                    ->select();
            } else {
                // 我的粉丝 意味着 别人是我的粉丝，busid = $busid ——> 找 fansid
                if ($busid != 0) {
                    $where['busid'] = $busid;
                }

                $list = $this->model
                    ->with(['fanslist'])
                    ->where($where)
                    ->order('id', 'desc')
                    ->limit($offset, $limit)
                    ->group('id')
                    ->select();

                // 修改二维数组键名（索引名称）
                // 将$list[0]['fanslist']改为$list[0]['business'] 方便前端for渲染数据
                foreach ($list as $value) {
                    $value['business'] = $value['fanslist'];
                    unset($value['fanslist']);
                };
            }

            if ($list) {
                $this->success('返回用户数据', null, $list);
                exit;
            } else {
                $this->error('暂无更多数据');
                exit;
            }
        }
    }
}
