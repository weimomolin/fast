//定义一个JS控制器 AMD require.js 模块化插件
//1、需要引入插件
//2、该控制器模块的业务逻辑
define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    //定义一个控制器
    var Controller = {
        index: function () {
            //给窗口修改大小
            $(".btn-add").data('area', ['50%', '90%'])
            //接收进入到章节界面的id值
            var ids = Fast.api.query('ids');

            // 初始化表格参数配置
            //配置整个表格中增删查改请求控制器地址，用的ajax的方式请求
            Table.api.init({
                extend: {
                    index_url: `subject/chapter/index?ids=${ids}`, // 列表查询的请求控制器方法
                    add_url: `subject/chapter/add?ids=${ids}`,     // 添加的控制器地址
                    edit_url: 'subject/chapter/edit',              // 编辑的控制器地址
                    del_url: 'subject/chapter/del',                // 删除的控制器地址
                    table: 'chapter',
                }
            });

            //获取view视图里面的dom元素table元素
            var table = $("#table")

            //渲染列表数据
            // $.ajax({
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url, //请求地址
                toolbar: ".toolbar", //工具栏
                pk: 'id', //默认主键字段名
                sortName: 'createtime', //排序的字段名
                sortOrder: 'desc', //排序的方式
                //渲染的数据部分
                columns: [ //渲染的字段部分
                    { checkbox: true },
                    { field: 'id', title: 'ID', operate: false, sortable: true },
                    { field: 'title', title: __('CapterTitle'), operate: 'LIKE' },
                    { field: 'url', title: __('CapterVideo'), operate: false, sortable: true },
                    {
                        field: 'createtime',
                        title: __('CreateTime'),
                        operate: 'RANGE',
                        addclass: 'datetimerange',
                        sortable: true,
                        formatter: Table.api.formatter.datetime
                    }
                ]
            })

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function()
        {
            //给控制器绑定通用事件
            Controller.api.bindevent()
        },
        edit: function()
        {
            //给控制器绑定通用事件
            Controller.api.bindevent()
        },
        del: function()
        {
            //给控制器绑定通用事件
            Controller.api.bindevent()
        },
        api: {
            //JS模块化的全局方法
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    }

    //模块返回值
    return Controller
})