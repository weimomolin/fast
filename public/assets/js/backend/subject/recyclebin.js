//定义一个JS控制器 AMD require.js 模块化插件
//1、需要引入插件
//2、该控制器模块的业务逻辑
define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    //定义一个控制器
    var Controller = {
        index: function () {
            // 初始化
            Table.api.init();
            //给选项卡绑定点击事件 切换选项卡的时候才会触发
            $(`a[data-toggle="tab"]`).on('shown.bs.tab', function () {
                var tab = $($(this).attr('href'))
                if (tab.length <= 0) return; //为空就停下

                //两个选项卡，分别给两个不同的请求方法
                //根据锚点切换，然后来触发选项卡的方法
                Controller.table[tab.attr('id')].call(this)
            })

            //一进来自动调用，第一个选项卡的方法
            Controller.table['subject']()
        },
        table: {
            //课程回收站
            subject: function () {
                //接收进入到详情界面的id值
                var ids = Fast.api.query('ids');

                // 初始化表格参数配置
                //配置整个表格中增删查改请求控制器地址，用的ajax的方式请求
                Table.api.init({
                    extend: {
                        subject_url: 'subject/recyclebin/recy_sub', // 课程
                        restore_url: 'subject/subject/restore', // 恢复
                        del_url: 'subject/subject/destroy', // 销毁
                        table: 'subject',
                    }
                });

                //获取view视图里面的dom元素table元素
                var SubjectTable = $("#SubjectTable");
                //渲染列表数据
                // $.ajax({
                SubjectTable.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.subject_url, //请求地址
                    toolbar: "#SubjectToolbar", //工具栏
                    pk: 'id', //默认主键字段名
                    sortName: 'createtime', //排序的字段名
                    sortOrder: 'desc', //排序的方式
                    //渲染的数据部分
                    columns: [ //渲染的字段部分
                        { checkbox: true },
                        { field: 'id', title: 'ID', operate: false, sortable: true },
                        { field: 'category.name', title: __('CateName'), operate: 'LIKE' },
                        { field: 'title', title: __('Titles'), operate: false, sortable: true },
                        {
                            field: 'thumbs_text',
                            title: __('Thumbs'),
                            operate: false,
                            formatter: Table.api.formatter.image
                        },
                        { field: 'price', title: __('Price'), operate: false, sortable: true },
                        {
                            field: 'createtime',
                            title: __('OrderTime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            sortable: true,
                            formatter: Table.api.formatter.datetime
                        },
                        //最后一排的操作按钮组
                        {
                            field: "operate",
                            title: __('Operate'),
                            table: SubjectTable,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate,
                            buttons: [
                                //定义自定义按钮
                                {
                                    name: 'restore', //跟table页面中绑定一样
                                    title: '恢复',
                                    icon: 'fa fa-circle-o-notch', //图标
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    url: $.fn.bootstrapTable.defaults.extend.restore_url,
                                    extend: "data-toggle='tooltip'",
                                    confirm: "是否确认恢复数据",
                                    success: function () {
                                        //刷新表格
                                        SubjectTable.bootstrapTable('refresh')
                                    }
                                },
                            ]
                        }
                    ]
                })
                // 还原，确认框的方法
                $('#SubjectToolbar .btn-reduction').on('click', function () {
                    let ids = Table.api.selectedids(SubjectTable);

                    layer.confirm('确定要还原吗?', { title: '还原', btn: ['是', '否'] }, function (index) {
                        //发送ajax请求
                        Backend.api.ajax(
                            //请求地址
                            { url: $.fn.bootstrapTable.defaults.extend.restore_url + `?ids=${ids}` },
                            //回调函数
                            function () {
                                // 关闭窗口
                                Layer.close(index)

                                // 刷新数据表格
                                SubjectTable.bootstrapTable('refresh')
                            }
                        )
                    });

                });
                // 为表格绑定事件
                Table.api.bindevent(SubjectTable);
            },


            //课程订单回收站
            order: function () {
                //接收进入到详情界面的id值
                var ids = Fast.api.query('ids');

                // 初始化表格参数配置
                //配置整个表格中增删查改请求控制器地址，用的ajax的方式请求
                Table.api.init({
                    extend: {
                        order_url: `subject/recyclebin/recy_order`, // 订单
                        restore_url: 'subject/order/restore', // 恢复
                        del_url: 'subject/order/destroy', // 销毁
                        table: 'order',
                    }
                });

                //获取view视图里面的dom元素table元素
                var OrderTable = $("#OrderTable");

                //渲染列表数据
                // $.ajax({
                OrderTable.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.order_url, //请求地址
                    toolbar: "#OrderToolbar", //工具栏
                    pk: 'id', //默认主键字段名
                    sortName: 'createtime', //排序的字段名
                    sortOrder: 'desc', //排序的方式
                    //渲染的数据部分
                    columns: [ //渲染的字段部分
                        { checkbox: true },
                        { field: 'id', title: 'ID', operate: false, sortable: true },
                        { field: 'subject.title', title: __('Titles'), operate: 'LIKE' },
                        { field: 'business.nickname', title: __('BusinessName'), operate: 'LIKE' },
                        { field: 'code', title: __('OrderCode') },
                        { field: 'total', title: __('Price'), operate: false, sortable: true },
                        {
                            field: 'createtime',
                            title: __('CreateTime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            sortable: true,
                            formatter: Table.api.formatter.datetime
                        },
                        //最后一排的操作按钮组
                        {
                            field: "operate",
                            title: __('Operate'),
                            table: OrderTable,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate,
                            buttons: [
                                //定义自定义按钮
                                {
                                    name: 'restore', //跟table页面中绑定一样
                                    title: '恢复',
                                    icon: 'fa fa-circle-o-notch', //图标
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    url: $.fn.bootstrapTable.defaults.extend.restore_url,
                                    extend: "data-toggle='tooltip'",
                                    confirm: "是否确认恢复数据",
                                    success: function () {
                                        //刷新表格
                                        OrderTable.bootstrapTable('refresh')
                                    }
                                }
                            ]
                        }
                    ]
                })
                // 还原，确认框的方法
                $('#OrderToolbar .btn-reduction').on('click', function () {
                    let ids = Table.api.selectedids(OrderTable);

                    layer.confirm('确定要还原吗?', { title: '还原', btn: ['是', '否'] }, function (index) {
                        //发送ajax请求
                        Backend.api.ajax(
                            //请求地址
                            { url: $.fn.bootstrapTable.defaults.extend.restore_url + `?ids=${ids}` },
                            //回调函数
                            function () {
                                // 关闭窗口
                                Layer.close(index)

                                // 刷新数据表格
                                OrderTable.bootstrapTable('refresh')
                            }
                        )
                    });

                });
                // 为表格绑定事件
                Table.api.bindevent(OrderTable);
            }
        },
        del: function () {
            //给控制器绑定通用事件
            Controller.api.bindevent()
        },
        api: {
            //JS模块化的全局方法
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    }
    //模块返回值
    return Controller
})