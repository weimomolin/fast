//定义一个JS控制器 AMD require.js 模块化插件
//1、需要引入插件
//2、该控制器模块的业务逻辑
define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function($, undefined, Backend, Table, Form){

    //定义一个控制器
    var Controller = {
        index: function()
        {
            //给选项卡绑定点击事件 切换选项卡的时候才会触发
            $(`a[data-toggle="tab"]`).on('shown.bs.tab', function(){
                var tab = $($(this).attr('href'))
                if(tab.length <= 0) return; //为空就停下

                //两个选项卡，分别给两个不同的请求方法
                //根据锚点切换，然后来触发选项卡的方法
                Controller.table[tab.attr('id')].call(this)
            })

            //一进来自动调用，第一个选项卡的方法
            Controller.table['order']()
        },
        table:{
            //课程订单
            order:function()
            {
                //接收进入到详情界面的id值
                var ids = Fast.api.query('ids');

                // 初始化表格参数配置
                //配置整个表格中增删查改请求控制器地址，用的ajax的方式请求
                Table.api.init({
                    extend: {
                        order_url: `subject/info/order?ids=${ids}`, //订单
                        table: 'subject_order',
                    }
                });

                //获取view视图里面的dom元素table元素
                var OrderTable = $("#OrderTable")
                
                //渲染列表数据
                // $.ajax({
                OrderTable.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.order_url, //请求地址
                    toolbar: "#OrderToolbar", //工具栏
                    pk: 'id', //默认主键字段名
                    sortName: 'createtime', //排序的字段名
                    sortOrder: 'desc', //排序的方式
                    //渲染的数据部分
                    columns: [ //渲染的字段部分
                        {field: 'id', title: 'ID', operate: false, sortable:true},
                        {field: 'code', title: __('OrderCode'), operate: 'LIKE'},
                        {field: 'total', title: __('OrderTotal'),operate: false, sortable:true},
                        {field: 'business.nickname', title: __('BusinessNickname'), operate: 'LIKE'},
                        {
                            field: 'createtime', 
                            title: __('OrderTime'), 
                            operate: 'RANGE', 
                            addclass: 'datetimerange', 
                            sortable:true,
                            formatter: Table.api.formatter.datetime
                        },
                    ]
                })

                // 为表格绑定事件
                Table.api.bindevent(OrderTable);
            },
            //课程评论
            comment:function()
            {
                //接收进入到详情界面的id值
                var ids = Fast.api.query('ids');

                // 初始化表格参数配置
                //配置整个表格中增删查改请求控制器地址，用的ajax的方式请求
                Table.api.init({
                    extend: {
                        comment_url: `subject/info/comment?ids=${ids}`, //评论
                        table: 'subject_comment',
                    }
                });

                //获取view视图里面的dom元素table元素
                var CommentTable = $("#CommentTable")
                
                //渲染列表数据
                // $.ajax({
                CommentTable.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.comment_url, //请求地址
                    toolbar: "#CommentToolbar", //工具栏
                    pk: 'id', //默认主键字段名
                    sortName: 'createtime', //排序的字段名
                    sortOrder: 'desc', //排序的方式
                    //渲染的数据部分
                    columns: [ //渲染的字段部分
                        {field: 'id', title: 'ID', operate: false, sortable:true},
                        {field: 'business.nickname', title: __('BusinessNickname'), operate: 'LIKE'},
                        {field: 'content', title: __('CommentContent'), operate: 'LIKE'},
                        {
                            field: 'createtime', 
                            title: __('CommentTime'), 
                            operate: 'RANGE', 
                            addclass: 'datetimerange', 
                            sortable:true,
                            formatter: Table.api.formatter.datetime
                        },
                    ]
                })

                // 为表格绑定事件
                Table.api.bindevent(CommentTable);
            },
        },
        api: {
            //JS模块化的全局方法
            bindevent: function () 
            {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    }

    //模块返回值
    return Controller
})