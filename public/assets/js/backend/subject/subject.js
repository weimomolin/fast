//定义一个JS控制器 AMD require.js 模块化插件
//1、需要引入插件
//2、该控制器模块的业务逻辑
define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function($, undefined, Backend, Table, Form){

    //定义一个控制器
    var Controller = {
        index: function()
        {
            //给窗口修改大小
            $(".btn-add").data('area', ['50%','90%'])

            // 初始化表格参数配置
            //配置整个表格中增删查改请求控制器地址，用的ajax的方式请求
            Table.api.init({
                extend: {
                    index_url: 'subject/subject/index', //列表查询的请求控制器方法
                    add_url: 'subject/subject/add', //添加的控制器地址
                    edit_url: 'subject/subject/edit', //编辑的控制器地址
                    del_url: 'subject/subject/del', //删除的控制器地址
                    info_url: 'subject/info/index', //课程详情地址
                    chapter_url: 'subject/chapter/index', //课程章节地址
                    table: 'subject',
                }
            });

            //获取view视图里面的dom元素table元素
            var table = $("#table")
            
            //渲染列表数据
            // $.ajax({
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url, //请求地址
                toolbar: ".toolbar", //工具栏
                pk: 'id', //默认主键字段名
                sortName: 'createtime', //排序的字段名
                sortOrder: 'desc', //排序的方式
                //渲染的数据部分
                columns: [ //渲染的字段部分
                    {checkbox: true},
                    {field: 'id', title: 'ID', operate: false, sortable:true},
                    {field: 'title', title: __('Titles'), operate: 'LIKE'},
                    {field: 'category.name', title: __('Cateid')},
                    {
                        field: 'thumbs_text', 
                        title: __('Thumbs'), 
                        operate: false,
                        formatter: Table.api.formatter.image
                    },
                    {field: 'price', title: __('Price'),operate: false, sortable:true},
                    {field: 'likes_text', title: __('Likes'),operate: false},
                    {
                        field: 'createtime', 
                        title: __('CreateTime'), 
                        operate: 'RANGE', 
                        addclass: 'datetimerange', 
                        sortable:true,
                        formatter: Table.api.formatter.datetime
                    },
                    //最后一排的操作按钮组
                    {
                        field: "operate", 
                        title: __('Operate'),
                        table: table,
                        events: Table.api.events.operate,
                        formatter: Table.api.formatter.operate,
                        buttons: [
                            //自定义按钮 课程详情
                            {
                                name: 'info',
                                title: function(data)
                                {
                                    return `${data.title}-课程详情`
                                },
                                icon: 'fa fa-ellipsis-h', //图标
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                url: $.fn.bootstrapTable.defaults.extend.info_url,
                                extend: 'data-toggle=\'tooltip\' data-area= \'["80%", "100%"]\'', //重点是这一句
                            },
                            //自定义按钮 课程章节
                            {
                                name: 'chapter',
                                title: function(data)
                                {
                                    return `${data.title}-课程章节`
                                },
                                icon: 'fa fa-bars', //图标
                                classname: 'btn btn-xs btn-info btn-dialog',
                                url: $.fn.bootstrapTable.defaults.extend.chapter_url,
                                extend: 'data-toggle=\'tooltip\' data-area= \'["80%", "100%"]\'', //重点是这一句
                            }
                        ]
                    }
                ]
            })

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function()
        {
            //给控制器绑定通用事件
            Controller.api.bindevent()
        },
        edit: function()
        {
            //给控制器绑定通用事件
            Controller.api.bindevent()
        },
        del: function()
        {
            //给控制器绑定通用事件
            Controller.api.bindevent()
        },
        api: {
            //JS模块化的全局方法
            bindevent: function () 
            {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    }

    //模块返回值
    return Controller
})