//定义一个JS控制器 AMD require.js 模块化插件
//1、需要引入插件
//2、该控制器模块的业务逻辑
define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    //定义一个控制器
    var Controller = {
        index: function () {
            //给窗口修改大小
            $(".btn-add").data('area', ['80%', '90%'])

            // 初始化表格参数配置
            //配置整个表格中增删查改请求控制器地址，用的ajax的方式请求
            Table.api.init({
                extend: {
                    index_url: 'business/highsea/index', //列表查询的请求控制器方法
                    aapply_url: 'business/highsea/aapply', //添加的控制器地址
                    recovery_url: 'business/highsea/recovery', //编辑的控制器地址
                    del_url: 'business/highsea/del', //删除的控制器地址
                    table: 'business',
                }
            });

            //获取view视图里面的dom元素table元素
            var table = $("#table")

            //渲染列表数据
            // $.ajax({
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url, //请求地址
                toolbar: ".toolbar", //工具栏
                pk: 'id', //默认主键字段名
                sortName: 'createtime', //排序的字段名
                sortOrder: 'desc', //排序的方式
                //渲染的数据部分
                columns: [ //渲染的字段部分
                    { checkbox: true },
                    { field: 'id', title: 'ID', operate: false, sortable: true },
                    { field: 'nickname', title: __('Nickname'), operate: 'LIKE' },
                    { field: 'source.name', title: __('SourceName'), operate: 'LIKE' },
                    { field: 'gender_text', title: __('Gender'), sortable: false, searchable: false },
                    { field: 'deal', title: __('Deal'), searchList: { "0": __('未成交'), "1": __('已成交') }, formatter: Table.api.formatter.normal },
                    { field: 'auth', title: __('Auth'), searchList: { "0": __('未认证'), "1": __('已认证') }, formatter: Table.api.formatter.normal },

                    {
                        field: "operate",
                        title: __('Operate'),
                        table: table,
                        events: Table.api.events.operate,
                        formatter: Table.api.formatter.operate,
                        buttons: [
                            {
                                name: 'apply',
                                icon: 'fa fa-arrow-down',
                                confirm: '确定要领取吗',
                                title: '领取',
                                extend: 'data-toggle="tooltip"',
                                classname: 'btn btn-xs btn-success btn-ajax',
                                url: 'business/highsea/apply?ids={id}',
                                success: function (data, ret) {
                                    $(".btn-refresh").trigger("click");
                                }
                            },
                            {
                                name: 'recovery',
                                title: '分配',
                                extend: 'data-toggle="tooltip"',
                                classname: 'btn btn-success btn-xs btn-dialog',
                                icon: 'fa fa-arrows-h',
                                url: 'business/highsea/recovery?ids={id}',
                            }
                        ]
                    }
                ]
            })
            // 领取
            $('.btn-process-1').on('click', function () {
                let ids = Table.api.selectedids(table);
                ids = ids.toString()
                layer.confirm('确定要领取吗?', { title: '领取', btn: ['是', '否'] },
                    function (index) {
                        $.post("business/highsea/apply", { ids: ids }, function (response) {
                            if (response.code == 1) {
                                Toastr.success(response.msg)
                                $(".btn-refresh").trigger('click');
                            } else {
                                Toastr.error(response.msg)
                            }
                        }, 'json');

                        layer.close(index)
                    }
                );

            });

            // 分配
            $('.btn-process-2').on('click', function () {
                let ids = Table.api.selectedids(table);
                ids = ids.toString()
                Fast.api.open($.fn.bootstrapTable.defaults.extend.recovery_url + "?ids=" + ids, '分配')
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        apply: function () {
            //给控制器绑定通用事件
            Controller.api.bindevent()
        },
        recovery: function () {
            //给控制器绑定通用事件
            Controller.api.bindevent()
        },
        del: function () {
            //给控制器绑定通用事件
            Controller.api.bindevent()
        },
        api: {
            //JS模块化的全局方法
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    }

    //模块返回值
    return Controller
})