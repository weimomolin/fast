//定义一个JS控制器 AMD require.js 模块化插件
//1、需要引入插件
//2、该控制器模块的业务逻辑
define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    //定义一个控制器
    var Controller = {
        index: function () {
            //给窗口修改大小
            $(".btn-add").data('area', ['80%', '90%'])

            // 初始化表格参数配置
            //配置整个表格中增删查改请求控制器地址，用的ajax的方式请求
            Table.api.init({
                extend: {
                    index_url: 'business/privatesea/index', // 私海列表首页
                    add_url: 'business/privatesea/add',// 添加界面
                    edit_url: 'business/privatesea/edit',// 编辑界面
                    del_url: 'business/privatesea/del',// 删除
                    multi_url: 'business/privatesea/multi', // 回收
                    table: 'business',
                }
            });

            //获取view视图里面的dom元素table元素
            var table = $("#table")

            //渲染列表数据
            // $.ajax({
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url, //请求地址
                toolbar: ".toolbar", //工具栏
                pk: 'id', //默认主键字段名
                sortName: 'createtime', //排序的字段名
                sortOrder: 'desc', //排序的方式
                //渲染的数据部分
                columns: [ //渲染的字段部分
                    { checkbox: true },
                    { field: 'id', title: 'ID', operate: false, sortable: true },
                    { field: 'nickname', title: __('Nickname'), operate: 'LIKE' },
                    { field: 'mobile', title: __('Mobile'), operate: 'LIKE' },
                    { field: 'email', title: __('Email'), operate: 'LIKE' },
                    { field: 'money', title: __('Money'), operate: 'LIKE' },
                    { field: 'source.name', title: __('SourceName'), operate: 'LIKE' },
                    { field: 'gender_text', title: __('Gender'), sortable: false, searchable: false },
                    { field: 'deal', title: __('Deal'), searchList: { "0": __('未成交'), "1": __('已成交') }, formatter: Table.api.formatter.normal },
                    { field: 'auth', title: __('Auth'), searchList: { "0": __('未认证'), "1": __('已认证') }, formatter: Table.api.formatter.normal },
                    { field: 'admin.username', title: __('AdminName'), operate: 'LIKE' },

                    {
                        field: "operate",
                        title: __('Operate'),
                        table: table,
                        events: Table.api.events.operate,
                        formatter: Table.api.formatter.operate,
                        buttons: [
                            {
                                name: 'business',
                                title: '客户详情',
                                extend: 'data-toggle="tooltip"',
                                classname: "btn btn-xs btn-primary btn-dialog vivst_test",
                                icon: 'fa fa-eye',
                                url: 'business/privateseainfo/index?ids={id}',
                            },
                            {
                                name: 'recovery', confirm: '确定要回收吗', title: '客户回收',
                                extend: 'data-toggle="tooltip"',
                                icon: 'fa fa-recycle',
                                classname: 'btn btn-xs btn-success btn-ajax',
                                url: 'business/privatesea/recovery?ids={id}',
                                success: function (data, ret) {
                                    $(".btn-refresh").trigger("click");
                                },
                                error: function (err) {
                                    console.log(err);
                                }
                            },
                        ]
                    }
                ]
            })
            // 领取
            $('.btn-process-1').on('click', function () {
                let ids = Table.api.selectedids(table);
                ids = ids.toString()
                layer.confirm('确定要领取吗?', { title: '领取', btn: ['是', '否'] },
                    function (index) {
                        $.post("business/highsea/apply", { ids: ids }, function (response) {
                            if (response.code == 1) {
                                Toastr.success(response.msg)
                                $(".btn-refresh").trigger('click');
                            } else {
                                Toastr.error(response.msg)
                            }
                        }, 'json');

                        layer.close(index)
                    }
                );

            });

            // 回收，确认框的方法
            $('.btn-reduction').on('click', function () {
                let ids = Table.api.selectedids(table);
                ids = ids.toString()
                layer.confirm('确定要回收吗?', { title: '回收', btn: ['是', '否'] },
                    function (index) {

                        $.post("business/privatesea/recovery", { ids: ids, action: 'success', reply: '' }, function (response) {
                            if (response.code == 1) {
                                Toastr.success(response.msg)
                                $(".btn-refresh").trigger('click');
                            } else {
                                Toastr.error(response.msg)
                            }
                        }, 'json');

                        layer.close(index);
                    }
                );

            });
            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {

            // 选中地区事件处理
            $('#region').on('cp:updated', function () {
                var citypicker = $(this).data("citypicker");
                var code = citypicker.getCode("district") || citypicker.getCode("city") || citypicker.getCode("province");
                $("#region-code").val(code);
            })

            Controller.api.bindevent();
        },
        edit: function () {
            // 选中地区事件处理
            $('#region').on('cp:updated', function () {
                var citypicker = $(this).data("citypicker");
                var code = citypicker.getCode("district") || citypicker.getCode("city") || citypicker.getCode("province");
                $("#region-code").val(code);
            })

            Controller.api.bindevent();
        },
        del: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    }

    //模块返回值
    return Controller
})