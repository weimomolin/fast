基于ThinkPHP5+FastaAdmin的云课堂项目


## 主要特性

* ThinkPhp5+FastaAdmin框架  
* MAV设计理念  

>>- 视图（View）：用户界面 html界面   
>>- 控制器（Controller）：业务逻辑   
>>- 模型（Model）：数据库增删查改  

## 项目目录结构
```
addons      插件目录
extend      拓展类的目录
runtime     我们在运行这个项目的时候，产生的缓存和编译文件
thinkphp    thinkphp框架的源代码目录 - 一般情况下不会去改这个目录
vendor/node_modules/  里面放的是插件的资源


application 是项目的应用目录
    config.php - init.php   系统配置文件
    database.php            数据库的配置文件
    每一个模块就是一个小型的 MVC
    admin/      后台模块
        controller 控制器
        model   模型
        view    视图
        lang    语言包
        validate    验证器
        config.php  模块的配置文件
        common.php / helper.php  封装的辅助函数

    index/      默认模块

    home/       自己创建这个模块 - 名字是自己定的 我们用来表示前台模块

    common/     公共模块

public      项目的公共资源目录
    assets/  放 图片 css js 字体文件
    uploads/  放上传文件存放目录
    erp.php  这个是后台的入口文件
    index.php 这个是前台的入口文件
    .htaccess apache伪静态文件
```

## 项目构建
### 1、搭建FastAdmin后台框架
>[FastAdmin下载地址](https://www.fastadmin.net/download.html)
>[fastadmin的详细安装过程](https://blog.csdn.net/weixin_43288600/article/details/121192252)

### 2、修改后台入口文件
默认自动创建的文件名称为随机的，不方便记忆，手动修改即可（注意路径）

![输入图片说明](md%E8%AF%B4%E6%98%8E%E7%9B%B8%E5%85%B3%E6%96%87%E4%BB%B6/md%E8%AF%B4%E6%98%8E_%E4%BF%AE%E6%94%B9%E5%90%8E%E5%8F%B0%E5%85%A5%E5%8F%A3%E6%96%87%E4%BB%B6.png)

### 3、验证站点
可能会遇到打开站点，访问失败问题  
解决方法：  
在 cmd 中 ping 网站站点，看是否为127.0.0.1的回复 是否有解析到本地  

![输入图片说明](md%E8%AF%B4%E6%98%8E%E7%9B%B8%E5%85%B3%E6%96%87%E4%BB%B6/md%E8%AF%B4%E6%98%8E_%E7%AB%99%E7%82%B9ping.png)

如果不是则去检查： 

![输入图片说明](md%E8%AF%B4%E6%98%8E%E7%9B%B8%E5%85%B3%E6%96%87%E4%BB%B6/md%E8%AF%B4%E6%98%8E_%E8%A7%A3%E5%86%B3%E7%AB%99%E7%82%B9%E8%AE%BF%E9%97%AE%E7%9A%84%E5%BC%82%E5%B8%B8.png)

### 4、开始编写界面
****
1、搭建模块  
php think build --module 模块名  
php think build --module admin   

2、创建控制器  
php think make:controller 模块名/控制器文件夹名称/控制器文件名   
php think make:controller admin/subject/Subject  

3、创建视图  
需要自己手动创建  
\application\admin\view\视图文件夹名称\视图文件名.html  
\application\admin\view\business\index.html  

4、创建模型  
php think make:model 模块名/模型文件夹名称/模型文件  
php think make:model common/Subject/Category  

5、JS控制器（一般来说 一个控制器对应一个JS控制器）  
手动创建  
public/assets/js/backend/控制器文件夹名称/控制器文件名.js 
public/assets/js/backend/subject/subject.js


### 5、一些注意事项
>  **菜单规则权限** (后台FastAdmin)  
所有的功能都必须要配权限  
菜单规则的地址 === 控制器的方法地址  

>  **修改默认指向模块**   
 访问前台：home模块 
 刚搭建的程序，没有自动指向到home模块，默认指向到index模块  
 修改application/config.php 全局配置文件 默认模块名 'default_module' => 'home', 修改为home模块   
 
>  **完整的访问地址** ：http://www.fast.com/入口文件/模块/控制器/控制器里面的方法   
>  **控制器文件** ：application/home/controller/Index.php  

>  **放前台的静态资源**  public/assets/home  
>  **修改控制器**  让我们自己的控制器 去 继承 thinkphp 底层的控制器 
```
//  **thinkphp 底层的控制器**   
use app\common\controller\Backend;  
class 控制器名 extends Backend {}  
```

>  **开启系统框架的错误信息显示**    
application/config.php  
```
// 应用调试模式
'app_debug'              => Env::get('app.debug', true), 
```
  
>  **清空缓存**   
1、浏览器的缓存  
2、框架的缓存 fast/runtime/ 清空这个文件夹里面的内容    

>  **语言包文件**   
\application\模块名\lang  

>  **save 和 insert**   
save 可以自动插入时间戳字段 支持validate验证器  
更新数据     isUpdate(true)->save($data)    
insert 不会  



****
## 项目要点
项目使用的技术文档

>[ **ThinkPHP5官方文档** ](https://www.kancloud.cn/manual/thinkphp5/118003)  
>[ **FastAdmin官方文档** ](https://doc.fastadmin.net/doc)  
>[ **EasyWeChat官方文档** ](https://easywechat.com/4.x/)  

###  1. 实现界面思路 

> 创建控制器 -> 控制器内继承ThinkPHP的构造方法 -> 编写界面渲染方法（一个方法名与界面名需保持一致）  
> 创建视图 -> `$this->view->fetch()` -> view/编写html文件部分  
> 创建模型 -> 根据项目需求，灵活将模型设置在common公共文件夹下或者自身模块文件夹下  

> [ **在控制器中使用视图的三种方法** ](https://www.jianshu.com/p/7647491656ad)
```
<?php
namespace app\home\controller;

// 引入底层控制器
use think\Controller;

// 继承ThinkPHP底层构造函数
class Index extends Controller
{
    public function __construct()
    {
        // 继承底层构造函数
        parent::__construct();

        // 公共区域中加载模型，那么下面所有的方法中都可以使用这个模型
        $this->BusinessModel = model('Business.Business');
    }

    // ---------- 主页逻辑 ----------
    public function index()
    {
        // 渲染一个模板页面
        return $this->view->fetch();
    }

    // ---------- 退出登录逻辑 ----------
    public function logout()
    {
        //清除cookie
        cookie('business', null);
        $this->success('退出成功', url('home/index/login'));
        exit;
    }
}
?>
```

###  2. 常用的一些方法  
// 继承底层构造函数  
parent::__construct();  

// 渲染一个模板页面  
return $this->view->fetch(); 
 
// 判断是否为POST请求  
$this->request->isPost(); 

// 接收POST请求传送过来的数据  
$this->request->param('参数名称', '默认值', '条件');  
`// $mobile = $this->request->param('mobile', '', 'trim');  `

// 数据库 查询单条数据  
model('模块地址')->where(查询条件)->value(返回字段名);  
`$data['sourceid'] = model('common/Business/Source')->where(['name' => ['LIKE', "%云课堂%"]])->value('id');`  
 
// 插入到数据库（1、创建客户模型 2、新建验证器，手动创建）  
`$result = $this->BusinessModel->validate('common/Business/Business')->save($data);`  

// save 和 insert 都是插入方法 但是有区别：  
// save 可以自动插入时间戳字段，而且也支持validate验证器的验证   
// save()传入id并设置isUpdate(true)后，可以实现更新数据功能   
 `$BusinessStatus = $this->BusinessModel->isUpdate(true)->save($data);`  
// insert 不会  

// 提交到cookie中  
cookie('起个名字'，放的数据)  
`cookie('business', $cookie); `  
// 清除cookie  
cookie('数据名'，null)   
`cookie('business', null);`  

// 是否有ajax请求过来  
$this->request->isAjax()  

****
### [**获取器**](https://www.kancloud.cn/manual/thinkphp5/135192) 
 获取器的作用是在获取数据的字段值后自动进行处理  
 相当于对拿得到数据库数据进行追加操作，但是不会影响到数据库  
 可以定义数据表中不存在的字段  

```
// fast\application\common\model\Business\Business.php 模型文件
// get（首字母大写 驼峰）Attr（$value,$data）  
// $value,当前字段的值  
// $data，整条数据  
// 增加的获取器字段选项
    protected $append = [
        'avatar_text',
    ];
    public function getAvatarTextAttr($value, $data){}
```

****
### [ **数据库事务** ](https://www.kancloud.cn/manual/thinkphp5/139063)
// 加载模型  
`$EmsModel = model('common/Ems');`  

// 开启事务   （可以进行数据回滚，相当于数据库撤销）  
加载模型 -> startTrans()  
`$EmsModel -> startTrans();`  

// 将事务提交  (提交的意思就是让刚刚插入的记录真实存在到数据表中)   
加载模型 -> commit()  
`$EmsModel->commit();`    

// 撤销回滚    (将刚才插入成功的验证码记录要撤销回滚)  
加载模型 -> rollback();   
`$EmsModel->rollback();`   


****
###  **模型定义后一些常用配置**   
```
class Business extends Model
{
    //标志当前模型操作的是哪张表
    protected $name = "business";

    //开启自动写入
    protected $autoWriteTimestamp = true;

    //设置字段的名字
    protected $createTime = "createtime"; //插入的时候设置的字段名

    //设置更新的字段名字
    protected $updateTime = false; //在更新数据的时候，不更新时间字段
}
```

****
### [**布局模板功能**](https://www.kancloud.cn/manual/thinkphp5/125013)  
ThinkPHP的模板引擎内置了布局模板功能支持，可以方便的实现模板布局以及布局嵌套功能  
抽离界面中的公共元素，例如header、footer、通用css、js部分
```
// fast\application\home\view\layout.html
// fast\application\home\view\Common\menu.html
```


****
### [**验证器**](https://www.kancloud.cn/manual/thinkphp5/129352)  
ThinkPHP5.0验证使用独立的\think\Validate类或者验证器进行验证   
关于表单数据的效验
application/common/validate/模块名/模型名  

```
// fast\application\common\validate\Business\Business.php  
/**
 * 定义客户表的验证器
 */
class Business extends Validate
{
    /**
     * 设置我们要验证字段的规则
     */
    protected $rule = [
        'mobile' => ['require', 'number', 'unique:business', 'regex:/(^1[3|4|5|7|8][0-9]{9}$)/'],
        'nickname' => ['require'],
        'password' => ['require'],
        'salt' => ['require'],
        'gender' => ['in:0,1,2'],
        'deal' => ['in:0,1'],
        'money' => ['number', '>=:0'],
        'email' => ['email', 'unique:business'],
        'auth' => ['in:0,1']
    ];

    /**
     * 设置错误的提醒信息
     */
    protected $message = [
        'mobile.require' => '手机号必填',
        'mobile.unique' => '手机号已存在，请重新输入',
        'mobile.regex' => '手机号码格式不正确',
        'password.require'  => '密码必填',
        'salt.require'      => '密码盐必填',
        'money.number'      => '余额必须是数字类型',
        'money.>='      => '余额必须大于等于0元',
        'auth.number'      => '认证状态的类型有误',
        'auth.in'      => '认证状态的值有误',
        'deal.number'      => '成交状态的类型有误',
        'deal.in'      => '成交状态的值有误',
        'nickname.require' => '昵称必填',
        'email.email' => '邮箱格式错误',
        'email.unique' => '邮箱已存在，请重新输入',
    ];

    /**
     * 设置验证器的场景
     */
    protected $scene = [
        'profile' => ['mobile', 'nickname', 'gender'],
    ];

    // 使用验证器的场景
    // 更新语句 如果是更新语句，需要给data提供一个主键id的值 这就是更新语句 
    // validate('common/Business/Business.profile')    通过  Business.profile   使用验证器的场景
    // $result = $this->BusinessModel->validate('common/Business/Business.profile')->isUpdate(true)->save($data);
}

```

****
### [**邮件验证码**](https://doc.fastadmin.net/doc/169.html)  
FastAdmin中的邮件发送采用phpmailer进行邮件发送，在使用邮件发送功能前请先在后台常规管理->系统配置中配置好邮件的相关信息

![输入图片说明](md%E8%AF%B4%E6%98%8E%E7%9B%B8%E5%85%B3%E6%96%87%E4%BB%B6/fast_md_%E9%82%AE%E7%AE%B1%E9%AA%8C%E8%AF%81%E7%A0%81%E9%85%8D%E7%BD%AE.png)

****
### 打印数据
单条打印：$单条数据集合->toArray();  
多条数据打印：collection(数据结果)->toArray();  


****
### 打印SQL语句  
// echo $this->model->getLastSql();

****
### apache 伪静态(404 Not 错误)  
http://www.fast.com/index.php/home/index/index 隐藏index.php  
http://www.fast.com/home/index/index  
修改 public/.htaccess  
```
<IfModule mod_rewrite.c>
  Options +FollowSymlinks -Multiviews
  RewriteEngine On

  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^(.*)$ index.php?/$1 [QSA,PT,L]
// 注意index.php后的 ？ 系统不同按需添加
</IfModule>
```

****
### 删除与软删除（数据安全性、恢复性）  
真正删除(DELETE)    
DELETE FROM 表名 where id = 1;  

软删除(UPDATE)  
在表中增加一个字段 deletetime 用来标记删除的时间  
删除的时候：执行是update语句  将当前的时间戳插入




